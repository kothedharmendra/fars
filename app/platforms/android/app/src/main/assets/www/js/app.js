var app = angular.module("fars", ["ngRoute","ui.bootstrap"]);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: 'welcome.html',
            controller: 'mainController'
        })
        .when("/login", {
            templateUrl: 'login.html',
            controller: 'loginController'
        })
        .when("/information", {
            templateUrl: 'information.html',
            controller: 'infoController'

        }).when("/lecture/:edid", {
            templateUrl: 'lecture.html',
            controller: 'editlecture'
        })
        .when("/report", {
            templateUrl: 'report.html',
            controller: 'reportController'
        })
        .when("/attendance", {
            templateUrl: 'attendance.html',
            controller: 'attendanceController'
        })
        .when("/question-set", {
            templateUrl: 'question-set.html',
            controller: 'questionSetController'
        })
        .when("/dashboard", {
            templateUrl: 'dashboard.html',
            controller: 'dashboardController'
        })
        .when("/contact", {
            templateUrl: 'contact-us.html',
            controller: 'contactController'
        }).otherwise({
        templateUrl: 'welcome.html',
        controller: 'mainController'
    });
});

// For MainPage: create the controller and inject Angular's $scope
app.controller('contactController', function ($scope, $window, $http, $rootScope,$location) {
    // create a message to display in our view

    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");
    $scope.branch_name = JSON.parse(localStorage.getItem("branch_name"));

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }

});

// For MainPage: create the controller and inject Angular's $scope
app.controller('attendanceController', function ($scope, $window, $http, $rootScope, $location, $filter) {
    // create a message to display in our view
    $scope.studentLoaded = false;
    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");
    $scope.branch_name = JSON.parse(localStorage.getItem("branch_name"));
    $scope.today_date = new Date();
    $scope.current_time = $filter('date')($scope.today_date, 'hh:mm:ss a');
    $scope.attendanceObj ={};
    $scope.attendanceObj.subjectObj= [];
    $scope.attendOptions = ['P','A'];
    $scope.totalPresentCount = 0;
    $scope.totalStudent = 0;

    $scope.getBranch = function () {
        $http({
            url: site_url + "appprocess_latest.php?opt=branch&teacher_id=" + $rootScope.teacher_id + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.branchname = data.branch.branchName;
            $scope.coursename = data.branch.courseName;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }

    /*
    * Submit Attendance
     */
    $scope.attendanceSubmit = function () {
        $scope.attendanceObj.teacher_id = localStorage.getItem("teacher_id");
        //$scope.attendanceObj.branch_name = JSON.parse(localStorage.getItem("branch_name"));
        $scope.attendanceObj.course_name = localStorage.getItem("course_name");
        $scope.attendanceObj.current_time = $scope.current_time;
        $scope.attendanceObj.today_date = $filter('date')($scope.today_date, "dd-MM-y");
        var data = $scope.attendanceObj.subjectObj;
        //alert(data.filter(function(o) { return o.a}).length);
        $http({
            url: site_url + "appprocess_latest.php?opt=studAttendance&time" + Math.random(),
            headers: {
                'Content-Type': 'application/json'
            },
            data: $scope.attendanceObj,
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                alert(data.message);
                $scope.attendanceObj = {};
                $scope.today_date = new Date();
                $scope.current_time = $filter('date')($scope.today_date, 'hh:mm:ss a');
                $scope.attendanceObj.teacher_id = $rootScope.teacher_id;
                $window.location.reload();
            } else {
                alert("Please select subject which is available");
               $window.location.reload();
            }
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection" + error);
           $window.location.reload();
        });
    }

    /*
    * Load Batch wise Student
     */
    $scope.loadStudent = function (subname) {
        $rootScope.teacher_id = localStorage.getItem("teacher_id");
       // $rootScope.branch_name = JSON.parse(localStorage.getItem("branch_name"));
        $rootScope.course_name = localStorage.getItem("course_name");
        $http({
            url: site_url + "appprocess_latest.php?opt=loadStudent&branchName=" + subname + "&courseName=" + $rootScope.course_name + "&teacher_id=" + $rootScope.teacher_id + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.studentData = data.subjectDetails;
            $scope.studentLoaded = true;
            $scope.totalStudent = $scope.studentData.length;
            $scope.totalPresentCount = $scope.studentData.length;
            // $scope.attendOptions =  data.attend;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.updateTotalCount = function (itemSelected) {
        if(itemSelected == 0)
            $scope.totalPresentCount = $scope.totalPresentCount + 1;
        else
            $scope.totalPresentCount = $scope.totalPresentCount - 1
    }


});
// For MainPage: create the controller and inject Angular's $scope
app.controller('questionSetController', function ($scope, $window, $http, $rootScope, $filter,$location) {

    /*
 * Load section and Question
  */
    $scope.getSection = function () {
        $http({
            url: site_url + "appprocess_latest.php?opt=section&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.sectionData = data;
            $scope.sectionLoaded = true;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    // create a message to display in our view
    $scope.getSection();
    $scope.sectionLoaded = false;
    $scope.selectedStudent = {};
    $scope.selectedStudent.fullname = "";
    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");
    $scope.branch_name = JSON.parse(localStorage.getItem("branch_name"));

    $scope.newUser = {};
    $scope.newUser.sectionDetails = [];
    $scope.today_date = new Date();
    $scope.start_time = $filter('date')($scope.today_date, 'hh:mm:ss a');
    $scope.end_time = $filter('date')($scope.today_date, 'hh:mm:ss a');

    var minDate = new Date();
    minDate.setDate(minDate.getDate() - $rootScope.past_date);
    $scope.minDateVal = minDate;
    $scope.maxDateVal = new Date();

    this.isSelected = function (checkTab) {
        return this.tab === checkTab;
    }

    $scope.IsVisible = false;
    $scope.showBox = function (res) {
        //If DIV is visible it will be hidden and vice versa.
        res.showBox = true;
        res.answer = "N";
    }

    $scope.hideBox = function (res) {
        //If DIV is visible it will be hidden and vice versa.
        res.showBox = false;
        res.answer = "Y";
    }

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }

    /*
    * Load Batch wise Student
     */
    $scope.loadStudent = function (subname) {
        $rootScope.teacher_id = localStorage.getItem("teacher_id");
        //$rootScope.branch_name = JSON.parse(localStorage.getItem("branch_name"));
        $rootScope.course_name = localStorage.getItem("course_name");
        $http({
            url: site_url + "appprocess_latest.php?opt=loadStudent&branchName=" + subname + "&courseName=" + $rootScope.course_name + "&teacher_id=" + $rootScope.teacher_id + "&time" + Math.random(),
        }).success(function (data) {
            $scope.studentData = data.subjectDetails;
            $scope.studentLoaded = true;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.getBranch = function () {
        $http({
            url: site_url + "appprocess_latest.php?opt=branch&teacher_id=" + $rootScope.teacher_id + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.branchname = data.branch.branchName;
            $scope.coursename = data.branch.courseName;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }



    $scope.submitQuestion= function()
    {
        $scope.newUser.teacher_id = localStorage.getItem("teacher_id");
        $scope.newUser.course_name = localStorage.getItem("course_name");
        $scope.newUser.start_time = $scope.start_time;
        $scope.newUser.roll_number = $scope.selectedStudent.roll_number;
        $scope.newUser.end_time = $scope.end_time;
        $scope.newUser.today_date = $filter('date')($scope.today_date, "dd-MM-y");
        $http({
            url: site_url + "appprocess_latest.php?opt=questionSubmit&time" + Math.random(),
            headers: {
                'Content-Type': 'application/json'
            },
            data: $scope.newUser,
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                alert("Thank you for submitting Answers");
                $scope.newUser = {};
                $scope.today_date = new Date();
                $scope.current_time = $filter('date')($scope.today_date, 'hh:mm:ss a');
                $scope.end_time = $filter('date')($scope.today_date, 'hh:mm:ss a');
                $scope.newUser.teacher_id = $rootScope.teacher_id;
               $window.location.reload();
            } else {
                alert("Please select subject which is available");
                $window.location.reload();
            }
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
            $window.location.reload();
        });
    }



});
// For MainPage: create the controller and inject Angular's $scope
app.controller('dashboardController', function ($scope, $window, $http, $rootScope, $location) {
    // create a message to display in our view

    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }

    $scope.dashBoardNumber = function () {
        $http({
            url: site_url + "appprocess_latest.php?opt=dashboardCount&&teacher_id=" + $rootScope.teacher_id +"&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.dashBoardData = data;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }


});
// For MainPage: create the controller and inject Angular's $scope
app.controller('mainController', function ($scope, $window, $http, $rootScope,$location) {
    // create a message to display in our view

    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }
        /*
     *Check no login redirect to login page
     */
    if ($rootScope.username == null || $rootScope.loggedIn== null || $rootScope.teacher_id== null) {
        $location.path('/login');
    } else {
        //alert($rootScope.username);
        $location.path('/dashboard');
    }

});

// For REPORT: create the controller and inject Angular's $scope
app.controller('reportController', function ($scope, $window, $http, $rootScope, $filter, $location) {
    // create a message to display in our view
    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");
    $rootScope.allow_pastdate = localStorage.getItem("allow_pastdate");
    $rootScope.past_date = localStorage.getItem("past_date");
    $rootScope.branch = localStorage.getItem("course_name");
    $scope.today_date = new Date();
    var minDate = new Date();
    minDate.setDate(minDate.getDate() - $rootScope.past_date);
    $scope.minDateVal = minDate;
    $scope.maxDateVal = new Date();

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }

    $scope.viewList = function (tday) {
        $scope.displayResult = true;
        $scope.selected_date = $filter('date')($scope.today_date, "dd-MM-y");
        $rootScope.teacher_id = localStorage.getItem("teacher_id");
        $http({
            url: site_url + "appprocess_latest.php?opt=listview&teacher_id=" + $rootScope.teacher_id + "&list=" + $scope.selected_date + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.resultData = data;
            $scope.displayResult = false;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.deleteInformation = function(id) {
            $http({
                url: site_url + "appprocess_latest.php?opt=deleteattendance&course=" + id + "&time" + Math.random(),
                method: "POST"
            }).success(function (data) {
                if (data.success) {
                    alert(data.message);
                    $location.path('/report');
                } else {
                    alert("Something went wrong.Please check internet Connection!!");
                    $location.path('/report');
                }
            }).error(function (error) {
                alert("Something went wrong.Please check internet Connection!!" + error);
            });
    }
});


// create the controller and inject Angular's $scope
app.controller('infoController', function ($scope, $window, $http, $rootScope, $filter, $location) {

    $rootScope.branches = ["Pune", "Nagpur"];
    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");
    $rootScope.allow_pastdate = localStorage.getItem("allow_pastdate");
    $rootScope.past_date = localStorage.getItem("past_date");
    $scope.topicsLoaded = false;

    //$scope.branch_name = localStorage.getItem("branch_name");
    //$rootScope.course_name = localStorage.getItem("course_name");

    $scope.branch_name = JSON.parse(localStorage.getItem("branch_name"));
    $scope.courseName = JSON.parse(localStorage.getItem("courseName"));
    //$rootScope.course_name = JSON.parse(localStorage.getItem("course_name"));
    $rootScope.subject = JSON.parse(localStorage.getItem("subject"));

    $scope.newUser = {};
    $scope.today_date = new Date();
    $scope.newUser.start_time = new Date();
    $scope.newUser.end_time = new Date();

    var minDate = new Date();
    $scope.maxDateVal = new Date();
    if ($rootScope.allow_pastdate == 1) {
        minDate.setDate(minDate.getDate() - $rootScope.past_date);
        $scope.minDateVal = minDate;
    } else {
        $scope.minDateVal = minDate;
    }

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }

    /*
      * Check Time
     */
    $scope.checkTime = function () {
        if ($scope.end_time && $scope.start_time.getHours() > $scope.end_time.getHours()) {
            alert("Start Time Cannot be Less Than EndTime");
            $scope.start_time = null;
            $scope.end_time = null;
        } else if ($scope.end_time && $scope.start_time.getHours() == $scope.end_time.getHours() && $scope.start_time.getMinutes() > $scope.end_time.getMinutes()) {
            alert("Start Time Cannot be Less Than EndTime");
            $scope.start_time = null;
            $scope.end_time = null;
        }
    }

    $scope.loadTopic = function (subname, courseName) {
        $rootScope.course_name = localStorage.getItem("course_name");
        $http({
            url: site_url + "appprocess_latest.php?opt=topic&sub=" + subname + "&course=" + courseName + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.topics = data;
            $scope.topicsLoaded = true;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.loadLectureNo = function (id) {
        $http({
            url: site_url + "appprocess_latest.php?opt=lecture&tid=" + id + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.lectureNo = data;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.getBranch = function () {
        $http({
            url: site_url + "appprocess_latest.php?opt=branch&teacher_id=" + $rootScope.teacher_id + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.branchname = data.branch.branchName;
            $scope.coursename = data.branch.courseName;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

    $scope.register = function () {
        $scope.newUser.teacher_id = $rootScope.teacher_id;
        $scope.newUser.start_time = $filter('date')($scope.start_time, "shortTime");
        $scope.newUser.end_time = $filter('date')($scope.end_time, "shortTime");
        $scope.newUser.date = $filter('date')($scope.newUser.today_date, "dd-MM-y");
        $scope.newUser.branch = localStorage.getItem("course_name");
        $http({
            url: site_url + "appprocess_latest.php?opt=add&time" + Math.random(),
            data: $scope.newUser,
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                alert("Thank you for submitting your report");
                $scope.newUser = {};
                $scope.today_date = new Date();
                $scope.newUser.start_time = new Date();
                $scope.newUser.end_time = new Date();
                $scope.newUser.no_lecture = 0;

                var minDate = new Date();
                $scope.maxDateVal = new Date();
                if ($rootScope.allow_pastdate == 1) {
                    minDate.setDate(minDate.getDate() - $rootScope.past_date);
                    $scope.minDateVal = minDate;
                } else {
                    $scope.minDateVal = minDate;
                }

                $scope.start_time = null;
                $scope.end_time = null;

                $scope.topicsLoaded = false;
                $window.location.reload();
            } else {
                alert("Invalid Credentials. Please try again.");
            }
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }
});

// create the controller and inject Angular's $scope
app.controller('loginController', function ($scope, $location, $http, $rootScope) {
    // create a message to display in our view
    //alert('Testing');

    $scope.user = {};
    $scope.userData = {};
    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");
    $rootScope.allow_pastdate = localStorage.getItem("allow_pastdate");
    $rootScope.past_date = localStorage.getItem("past_date");

    $scope.login = function () {

        $http({
            url: site_url + "appprocess_latest.php?opt=login&time" + Math.random(),
            data: {'id': $scope.user.id, 'password': $scope.user.password},
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                //$scope.getUserData($scope.user.id);
                $scope.userData = data;
                localStorage.setItem("loggedIn", "true");
                $rootScope.loggedIn = true;
                localStorage.setItem("username", $scope.userData.name);
                $rootScope.username = $scope.userData.name;
                localStorage.setItem("teacher_id", $scope.userData.teacher_id);
                $rootScope.teacher_id = $scope.userData.teacher_id;
                localStorage.setItem("branch_name", JSON.stringify($scope.userData.branch_name));
                $scope.branch_name = $scope.userData.branch_name;
                localStorage.setItem("course_name", $scope.userData.course_name);
                $rootScope.course_name = $scope.userData.course_name;
                localStorage.setItem("subject", JSON.stringify($scope.userData.subject));
                $rootScope.subject = $scope.userData.subject;
                localStorage.setItem("allow_pastdate", $scope.userData.allow_pastdate);
                localStorage.setItem("past_date", $scope.userData.past_date);
                $rootScope.allow_pastdate = localStorage.getItem("allow_pastdate");
                $rootScope.past_date = localStorage.getItem("past_date");
                localStorage.setItem("courseName", JSON.stringify($scope.userData.courseName));
                $rootScope.courseName = $scope.userData.courseName;
                $location.path('/dashboard');
                $scope.user = {};

            } else {
                alert("Invalid Credentials. Please try again.");
                $scope.user = {};
            }
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }
})

// create the controller and inject Angular's $scope
app.controller('editlecture', function ($scope, $window, $http, $rootScope, $filter, $location,$routeParams) {
    $scope.tutorialid = $routeParams.edid;
    $scope.lecture ={};
    $rootScope.loggedIn = localStorage.getItem("loggedIn");
    $rootScope.username = localStorage.getItem("username");
    $rootScope.teacher_id = localStorage.getItem("teacher_id");

    $scope.logout = function () {
        localStorage.removeItem("loggedIn");
        localStorage.removeItem("username");
        localStorage.removeItem("teacher_id");
        $rootScope.username = null;
        $rootScope.loggedIn = null;
        $rootScope.teacher_id = null;
        $location.path('/');
    }


    if($scope.tutorialid > 0){
        $http({
            url: site_url + "appprocess_latest.php?opt=editlecture&tid=" + $scope.tutorialid + "&time" + Math.random(),
            method: "GET"
        }).success(function (data) {
            $scope.dataform = data;
            var json = $scope.dataform.result;
            $scope.lecture.present_head_count = json.present_head_count;
            $scope.lecture.lecture_no = json.lecture_no;
            $scope.lecture.mentoring_count = json.mentoring_count;
            $scope.lecture.total_lecture_no = json.total_lecture_no;
            $scope.lecture.doubt_clearing_count = json.doubt_clearing_count;
            $scope.lecture.subject = json.subject;
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    } else {
        $location.path('/dashboard');
    }

    /*
    * Update Lecture Information
     */
    $scope.update = function () {
        $scope.lecture.tutorialid = $scope.tutorialid;
        $scope.lecture.teacher_id = $rootScope.teacher_id;
        $http({
            url: site_url + "appprocess_latest.php?opt=add&time" + Math.random(),
            data: $scope.lecture,
            method: "POST"
        }).success(function (data) {
            if (data.success) {
                alert("Information Updated successfully");
                $location.path('/report');
            } else {
                alert("Invalid Credentials. Please try again.");
            }
        }).error(function (error) {
            alert("Something went wrong.Please check internet Connection!!" + error);
        });
    }

});
