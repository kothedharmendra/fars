<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

// Sanitize if you want
$customer_id = filter_input(INPUT_GET, 'question_id', FILTER_VALIDATE_INT);
$operation = filter_input(INPUT_GET, 'operation',FILTER_SANITIZE_STRING); 
($operation == 'edit') ? $edit = true : $edit = false;

//Handle update request. As the form's action attribute is set to the same script, but 'POST' method, 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Get customer id form query string parameter.
    $customer_id = filter_input(INPUT_GET, 'question_id', FILTER_SANITIZE_STRING);
    //Get input data
    $data_to_update = filter_input_array(INPUT_POST);
    $data_to_update['updated_at'] = date('Y-m-d H:i:s');
    $db->where('id',$customer_id);
    $data_to_update['question_name'] = $_POST['question_name'];
    $data_to_update['status'] = $_POST['status'];
    //$data_to_update['topic_name'] = implode( ',',$data_to_update['topic_name']);
    $stat = $db->update('question', $data_to_update);
    if($stat)
    {
        $_SESSION['success'] = "Question updated successfully!";
        //Redirect to the listing page,
        header('location: question.php');
        //Important! Don't execute the rest put the exit/die. 
        exit();
    }
}

//If edit variable is set, we are performing the update operation.
if($edit)
{
    $db->where('id', $customer_id);
    //Get data to pre-populate the form.
    $question = $db->getOne("question");
}
?>
<?php include_once 'includes/header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <h2 class="page-header">Update Question</h2>
    </div>
    <!-- Flash messages -->
    <?php
        include('./includes/flash_messages.php')
    ?>
    <form class="" action="" method="post" enctype="multipart/form-data" id="teacher_form">
        <fieldset>
    <div class="form-group">
        <label for="topic_name">Question *</label>
        <textarea name="question_name" placeholder="Enter Question" class="form-control" required="required"
                  id="question_name"><?php echo $edit ? $question['question_name'] : ''; ?></textarea>
    </div>
    
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $question['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $("#question_form").validate({
       rules: {
           question_name: {
                required: true,
                minlength: 3
            },   
        }
    });
});
</script>
<?php include_once 'includes/footer.php'; ?>