<?php 
// Report all PHP errors
error_reporting(-1);

session_start();
require_once './config/config.php';

function array_flatten($array) { 
  if (!is_array($array)) { 
    return false; 
  } 
  $result = array(); 
  foreach ($array as $key => $value) { 
    if (is_array($value)) { 
      $result = array_merge($result, array_flatten($value)); 
    } else { 
      $result[$key] = $value; 
    } 
  } 
  return $result; 
}
$link = mysqli_connect( $servername, $username, $password, $dbname );
if (!$link) {
    die( mysqli_error() );
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
	<style type="text/css">
		body{
			font-family: 'Roboto Condensed', sans-serif;
     font-weight: 400;
     font-size: 16px;
     background-color: #f4f4f4;
		}
		h3,h4{
			margin: 0px;
			padding-top: 10px;
		}
		th{
			background-color: #666;
			color: #fff;
		}
		th h3{
			color: #fff;
			padding: 10px;
		}

	</style>
</head>
<body>
	<table style="width: 100%;max-width: 900px;margin-left: auto;margin-right: auto;border-radius: 10px;background-color: #fff;padding: 10px;">
		<tr>

			<td>
				<table style="width: 100%">
					<tr>
						<td><img src="http://www.icadiit.com/images/logopng.png" height="50"></td>
						<td><h2 style="text-align: right;">STUDENT MENTORING</h2></td>
					</tr>		
				</table>
			<td>

		</tr>
		<tr>
			<td>
				<hr>
				<table style="width: 100%">
				<?php 
			
				$sql1="select * from student where roll_number='".$_POST["roll_number"]."'";
$res1=mysqli_query($link,$sql1);
$row1=mysqli_fetch_array($res1);

				?>
					<tr>
						<td>
							<strong>Student Name:</strong><?php echo $row1["firstname"]." ".$row1["middlename"]." ".$row1["lastname"] ?>
						</td>
						<td><strong>Roll No.:</strong> <?php echo $row1["roll_number"]?></td>
						<td><strong>Batch:</strong> <?php echo $row1["branch_name"]?></td>
					</tr>
					<?php
					$arr=explode("^", $_POST["mentoring"]);
					$arr1=explode("#", $arr[1]);
					$sql_teacher="select * from teacher where id='".$arr1[0]."'";
$res_teacher=mysqli_query($link,$sql_teacher);
$row_teacher=mysqli_fetch_array($res_teacher);

	?>
					<tr>
						<td>
							<strong>Subject:</strong><?php echo $row_teacher["subject"]?>
						</td>

						<td>
							<strong>Faculty Name:</strong><?php echo $row_teacher["firstname"]." ".$row_teacher["lastname"]?>
						</td>

						<td>
							<strong>Date:</strong><?php echo date('d M Y', strtotime($arr[0]))." ".$arr1[1]." ".$arr1[2]?>
						</td>
					</tr>
				
					<tr>
						<td class="3">
							<p></p>
						</td>
					</tr>
				</table>
			<td>
		</tr>
		<?php
		$sql="select distinct section_name,question_data,roll_number,course_name,student_name,teacher_id,today_date,start_time,end_time from mentoring where roll_number='".$_POST["roll_number"]."' and  today_date='".$arr[0]."' and teacher_id='".$arr1[0]."' order by id";
$res=mysqli_query($link,$sql);
$k=0;
while($row=mysqli_fetch_array($res))
{
	if($row["question_data"]!="")
	{
	$k++;
	//echo $row["section_name"];
        ?>
		<tr>
			<th>
				<h3>Section <?php echo $k?>) <?php echo $row["section_name"]?></h3>
			</th>
		</tr>
		<?php
		$sql_ques="select question_data,concat(left(subject, 1),'',teacher_id,'-',date_format(today_date,'%d %b %y'),'-',start_time,'-',end_time) as teacher_code  from mentoring,teacher where teacher_id=teacher.id and section_name like '%".$row["section_name"]."%' and roll_number = '".$_POST["roll_number"]."' and teacher_id='".$arr1[0]."' and today_date='".$arr[0]."' order by mentoring.id desc";
	
	$res_ques=mysqli_query($link,$sql_ques);
	$row_ques=mysqli_fetch_array($res_ques);
	$ques_data=$row_ques["question_data"];
	$arr_ques=json_decode($ques_data);
	//echo count($arr_ques);
	//echo $arr_ques[1]->question_name;
	if($ques_data!="")
	{
	
	//$rowData.= "Teacher Code \t Question " . "\t" . "Answer " . "\t" . "Comment"."\n";
	for($i=0;$i<count($arr_ques);$i++)
	{
		if($arr_ques[$i]->question_name!="")
		{
				//echo "asdasd";
		?>

<tr>
			<td>
				<h4><?php echo $arr_ques[$i]->question_name;?></h4>
				<p><?php echo $arr_ques[$i]->answer?></p>
				<p><?php echo $arr_ques[$i]->comment?></p>
			</td>
		</tr>
		<?php
	}}}
		}}?>
		
		<!-- <tr>
			<td>
				<h4>b) Genuine Remarks & Summary</h4>
				<p>Aenean euismod ex a condimentum dictum. </p>
			</td>
		</tr>
		<tr>
			<td>
				<h4>c) In case of non-compliance, try to get reasons & make him understand importance.</h4>
				<p>Consectetur adipiscing elit. Donec auctor euismod leo, id cursus nisi ornare at.</p>
			</td>
		</tr>
		<tr>
			<td>
				<h4>d) In case of compliance, try to get ACTIONS taken by student. Help him to reach conclusions & educate him to make use in examination/solving with respect to cross-checking.</h4>
				<p>Aenean euismod ex a condimentum dictum. Proin viverra consequat rhoncus. </p>
			</td>
		</tr>
		<tr>
			<th>
				<h3>Section 1) TAB Status: Refer instructions on last cover page</h3>
			</th>
		</tr>
		<tr>
			<td>
				<h4>a) Regular & complete</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec auctor euismod leo, id cursus nisi ornare at. Morbi fringilla nibh arcu, a sollicitudin dui fermentum iaculis. Nullam sollicitudin leo eu libero ultrices tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis rutrum et dui eget lobortis. Aenean euismod ex a condimentum dictum. Proin viverra consequat rhoncus. Nunc non est quis eros suscipit sagittis vitae sed diam. Quisque pulvinar at est sed posuere. Nunc vitae malesuada dolor. </p>
			</td>
		</tr>
		<tr>
			<td>
				<h4>b) Genuine Remarks & Summary</h4>
				<p>Aenean euismod ex a condimentum dictum. Proin viverra consequat rhoncus. Nunc non est quis eros suscipit sagittis vitae sed diam. Quisque pulvinar at est sed posuere. Nunc vitae malesuada dolor. </p>
			</td>
		</tr>
		<tr>
			<td>
				<h4>c) In case of non-compliance, try to get reasons & make him understand importance.</h4>
				<p>Consectetur adipiscing elit. Donec auctor euismod leo, id cursus nisi ornare at. Morbi fringilla nibh arcu, a sollicitudin dui fermentum iaculis. Nullam sollicitudin leo eu libero ultrices tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis rutrum et dui eget lobortis. Aenean euismod ex a condimentum dictum. Proin viverra consequat rhoncus. Nunc non est quis eros suscipit sagittis vitae sed diam. Quisque pulvinar at est sed posuere. Nunc vitae malesuada dolor. </p>
			</td>
		</tr>
		<tr>
			<td>
				<h4>d) In case of compliance, try to get ACTIONS taken by student. Help him to reach conclusions & educate him to make use in examination/solving with respect to cross-checking.</h4>
				<p>Aenean euismod ex a condimentum dictum. Proin viverra consequat rhoncus. Nunc non est quis eros suscipit sagittis vitae sed diam. Quisque pulvinar at est sed posuere. Nunc vitae malesuada dolor. </p>
			</td>
		</tr> -->

	</table>
</body>
</html>