<?php
session_start();
session_destroy();

if(isset($_COOKIE['email']) && isset($_COOKIE['password'])){
	unset($_COOKIE['email']);
    unset($_COOKIE['password']);
    setcookie('email', null, -1, '/');
    setcookie('password', null, -1, '/');
}
header('Location:index.php');

 ?>