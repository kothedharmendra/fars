<?php
header( "Access-Control-Allow-Origin: *" );
header( "Access-Control-Allow-Methods: PUT, GET, POST" );
header( 'Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept' );

require_once '../config/config.php';
$link = mysqli_connect( $servername, $username, $password, $dbname );
if (!$link) {
    die( mysqli_error( $link ) );
}
if (isset( $_GET["opt"] ) && !empty( $_GET["opt"] )) {
    $opt = $_GET["opt"];
} else {
    $opt = "";
}

if (isset( $_GET["sub"] ) && !empty( $_GET["sub"] )) {
    $sub = ucwords( $_GET["sub"] );
}

if (isset( $_GET["teacher_id"] ) && !empty( $_GET["teacher_id"] )) {
    $teacherId = $_GET["teacher_id"];
}

if (isset( $_GET["list"] ) && !empty( $_GET["list"] ) && $_GET["list"] != 'undefined') {
    $list = $_GET["list"];
} else {
    $list = date( 'd-m-Y' );
}

if (isset( $_GET["tid"] ) && !empty( $_GET["tid"] )) {
    $taughtId = $_GET["tid"];
}

if (isset( $_GET["course"] ) && !empty( $_GET["course"] )) {
    $course = $_GET["course"];
}

if (isset( $_GET["branchName"] ) && !empty( $_GET["branchName"] )) {
    $branchName = $_GET["branchName"];
}

if (isset( $_GET["courseName"] ) && !empty( $_GET["courseName"] )) {
    $courseName = $_GET["courseName"];
}

switch ($opt) {
    case("login"):
        $res = loginAction( $link );
        print_r( $res );
        break;
    case("add"):
        $res = addAction( $link );
        print_r( $res );
        break;

    case("topic"):
        $res = getTopicAction( $link, $course, $sub );
        print_r( $res );
        break;

    case("lecture"):
        $res = getTopicNumber( $link, $taughtId );
        print_r( $res );
        break;

    case("listview"):
        $res = getViewAction( $link, $teacherId, $list );
        print_r( $res );
        break;

     case("deleteattendance"):
        $res = deleteAttendance($link, $course);
        print_r( $res );
        break;
     case("editlecture"):
        $res = editLecture($link, $taughtId);
        print_r($res);
        break;
    case("loadStudent"):
        $res = getStudentByBranchName( $link, $courseName,$branchName,$teacherId );
        print_r( $res );
        break;

    case("section"):
        $res = getSection( $link );
        print_r( $res );
        break;

    case("studAttendance"):
        $res = addStudAttendance( $link );
        print_r( $res );
        break;
    case("questionSubmit"):
        $res = submitQuestion($link);
        print_r($res);
        break;
    case("dashboardCount"):
        $res = getDashBoardCount($link, $teacherId);
        print_r($res);
        break;

    default:
}

/**
 * @param $link
 */
function addAction($link)
{
    $postData = file_get_contents( "php://input" );
    $request = json_decode($postData);
    $edidId = $request->tutorialid;
    $teacherId = $request->teacher_id;
    $presentHeadCount = $request->present_head_count;
    $lectureNo = $request->lecture_no;
    $mentoringCount = $request->mentoring_count;
    $total_lecture_no = $request->total_lecture_no;
    $doubtClearingCount = $request->doubt_clearing_count;
    //INSERT STUDENT INFORMATION
    if($edidId > 0) {
       $sql = "UPDATE attendance SET
          `present_head_count` ='$presentHeadCount',
          `doubt_clearing_count` ='$doubtClearingCount',
          `lecture_no` ='$lectureNo',
		  `total_lecture_no` ='$total_lecture_no',
          `mentoring_count` ='$mentoringCount' 
          WHERE teacher_id ='$teacherId' AND id='$edidId'";
    } else {
        $batch = $request->batch;
        $date = date( 'Y-m-d', strtotime( $request->date ) );
        $subject = $request->subject;
        $topicTaught = $request->topic_taught;
        $startTime = $request->start_time;
        $endTime = $request->end_time;
        $branch = $request->branch;
        $totalLectureNo = getTotalTopicNumber( $link, $topicTaught );
        $sequenceNo = getSeqenceNumber( $link, $topicTaught );//$request->sequence_no;
        $balenceLectureNo = getBalance( $totalLectureNo, $lectureNo );
        $sql = "INSERT INTO attendance SET
          `teacher_id` ='$teacherId',
          `batch` ='$batch',
          `branch` ='$branch',
          `date` ='$date',`subject` ='$subject',`topic_taught` ='$topicTaught',
          `present_head_count` ='$presentHeadCount',
          `doubt_clearing_count` ='$doubtClearingCount',
		  `sequence_no` ='$sequenceNo',
		  `total_lecture_no` ='$totalLectureNo',
          `mentoring_count` ='$mentoringCount',
          `start_time` ='$startTime',`end_time` ='$endTime',
          `lecture_no` ='$lectureNo',
          `balence_lecture` ='$balenceLectureNo',
		  `created_at`=now()";
    }

    $result = mysqli_query( $link, $sql );

    if ($result) {
        $data['success'] = true;
        $data['message'] = 'Data Inserted/Updated successfully!';
    } else {
        // if there are no errors, return a message
        $data['success'] = false;
        $data['message'] = 'Error In Form Submission, Please try once Again!';
    }
    echo json_encode( $data );
}

/**
 * @param $link
 */
function loginAction($link)
{
    $postData = file_get_contents( "php://input" );
    $request = json_decode( $postData );
    if ($request) {

        $email = strtolower( trim( $request->id ) );
        $password = md5( trim( $request->password ) );
        $sql = "SELECT * FROM teacher WHERE `email`='$email' AND `password`='$password'";
        $getID = mysqli_fetch_assoc( mysqli_query( $link, $sql ) );
        $rowId = $getID['id'];
        $rowStatus = $getID['status'];
        $rowIsDelete = $getID['is_delete'];
        if ($rowStatus != '0' && $rowIsDelete == '0') {
            if ($rowId > 0) {
                $data['success'] = true;
                $data['name'] = ucfirst( $getID['firstname'] . " " . $getID['lastname'] );
                $data['teacher_id'] = $rowId;
                $data['allow_pastdate'] = $getID['allow_pastdate'];
                $data['past_date'] = '6';
                $data['branch_name'] = explode( ',', $getID['branch_name'] );
                $data['course_name'] = $getID['course_name'];
                $data['courseName'] = explode(',', $getID['course_name']);
                $data['subject'] = explode(',', $getID['subject']);
                $data['message'] = 'Login successfully!';
            } else {
                // if there are no errors, return a message
                $data['success'] = false;
                $data['message'] = 'Incorrect Username/Password';
            }
        } else {
            // if there are no errors, return a message
            $data['success'] = false;
            $data['message'] = 'Sorry your account is Disable';
        }
        echo json_encode($data);
    }
}

/**
 * @param $link
 * @param $course
 * @param $sub
 */
function getTopicAction($link, $course, $sub)
{
    $listArray = array_map( 'trim', explode( ',', $course ) );
    $listWhere = "'" . implode( "','", $listArray ) . "'";
    $data = array();
    $sql = "SELECT topic_name,id FROM course WHERE `course_year` IN($listWhere) AND subject='" . $sub . "' AND `status`='1' GROUP BY topic_name order by id";
    $result = mysqli_query( $link, $sql );
    if ($result) {
        while ($row = mysqli_fetch_assoc( $result )) {
            $data[] = $row;
        }
    } else {
        // if there are no errors, return a message
        $data["topic"] = 'No Topic For this Subject';
    }
    echo json_encode( $data );
}

//View Report
/**
 * @param $link
 * @param $teacherId
 * @param $list
 */
function getViewAction($link, $teacherId, $list)
{
    try {
        $list = date( 'Y-m-d', strtotime( $list ) );
        $data = array();
        $sql = "SELECT a.* , b.topic_name AS topic_taught FROM attendance AS a 
				INNER JOIN course AS b ON a.topic_taught=b.id
				WHERE a.teacher_id='" . $teacherId . "' AND a.date='" . $list . "' AND a.status=1";
        $result = mysqli_query( $link, $sql );
        if ($result) {
            while ($row = mysqli_fetch_assoc( $result )) {
                $data[] = $row;
                //$data[0]['date'] = date("d-m-Y", strtotime($row['date']));
            }
        } else {
            $data['error'] = 'No attendance Submitted!';
        }
        //print_r($data);exit;
        echo json_encode( $data );
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

//Get Topic Number
/**
 * @param $link
 * @param $id
 */
function getTopicNumber($link, $id)
{
    try {
        $sql = "SELECT seq_no,no_lecture, id FROM course WHERE `id`='$id'";
        $getID = mysqli_fetch_assoc( mysqli_query( $link, $sql ) );
        $rowId = $getID['id'];
        if ($rowId > 0) {
            $data['no_lecture'] = $getID['no_lecture'];
            $data['seq_no'] = $getID['seq_no'];
        } else {
            // if there are no errors, return a message
            $data['no_lecture'] = '0';
            $data['seq_no'] = '0';
        }
        echo json_encode( $data );
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}


//Get Topic Number
/**
 * @param $link
 * @param $id
 * @return int|string
 */
function getTotalTopicNumber($link, $id)
{
    $data = 0;
    try {
        $sql = "SELECT no_lecture, id FROM course WHERE `id`='$id'";
        $getID = mysqli_fetch_assoc( mysqli_query( $link, $sql ) );
        $rowId = $getID['id'];
        if ($rowId > 0) {
            $data = $getID['no_lecture'];
        } else {
            // if there are no errors, return a message
            $data = '0';
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}


//Get Sequence Number
/**
 * @param $link
 * @param $id
 * @return int|string
 */
function getSeqenceNumber($link, $id)
{
    $data = 0;
    try {
        $sql = "SELECT seq_no, id FROM course WHERE `id`='$id'";
        $getID = mysqli_fetch_assoc( mysqli_query( $link, $sql ) );
        $rowId = $getID['id'];
        if ($rowId > 0) {
            $data = $getID['seq_no'];
        } else {
            // if there are no errors, return a message
            $data = '0';
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

/**
 * @param $total
 * @param $present
 * @return string
 */
function getBalance($total, $present)
{
    $result = '';
    if ($total >= $present) {
        $result = $total - $present;
    } else if ($total < $present) {
        $result = '+ ' . ($present - $total);
    }
    return $result;
}

/**
 * @param $link
 * @param $courseName
 */
function getStudentByBranchName($link, $courseName, $branchName, $teacherId)
{
    try {
        $data = array();
        $sql = " SELECT a.stud_id, a.roll_number, CONCAT(a.firstname,' ',a.lastname) AS studentName , LEFT(b.subject , 1) AS subjectName,subject
                ,CONCAT(b.firstname,' ',b.lastname) AS facultyName,b.id AS teacher_id 
                FROM `student` AS a INNER JOIN `teacher` AS b
                ON b.branch_name LIKE  CONCAT('%',a.branch_name,'%')
                WHERE a.branch_name='".$branchName."' AND a.status=1 AND b.id='".$teacherId."' order by a.firstname";
        $result = mysqli_query( $link, $sql );
        if ($result) {
            while ($row = mysqli_fetch_assoc( $result )) {
                $data['subjectDetails'][] = $row;
                $data['attend'] = array($row['subjectName'],'A');
            }
        }
        echo json_encode($data);
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

/**
 * @param $link
 */
function getSection($link)
{
    try {
        $data = array();
        $sql = "SELECT id, section_name,master_question_id FROM `section` WHERE status=1";
        $result = mysqli_query($link, $sql);
        if ($result) {
            while ($row = mysqli_fetch_assoc($result)) {
                $data [$row['id']] = array(
                    'sectionId' => $row['id'],
                    'sectionName' => $row['section_name'],
                    'questionList' => getAllQuestion($link, $row['master_question_id'])
                );
            }
        } else {
            $data['error'] = 'No attendance Submitted!';
        }
        echo json_encode($data, true);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}


/**
 * @param $link
 * @param $masterId
 * @return array
 */
function getAllQuestion($link, $masterId)
{
    try {
        $data = array();
        $sql = "SELECT id, question_name FROM `question` WHERE id IN ($masterId) AND status=1";
        $result = mysqli_query( $link, $sql );
        if ($result) {
            while ($row = mysqli_fetch_assoc( $result )) {
                $data [] = $row;
            }
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

/**
 * @param $link
 * Submit Attendance
 */
function addStudAttendance($link)
{
    $postData = file_get_contents( "php://input" );
    $request = json_decode($postData);
    $StudRequests = json_decode(json_encode($request->subjectObj), true);
	$results = array();
	$insertData = array();
	foreach ($StudRequests  as $studRequest) {
		//print_r($studRequest);
		$results [] = $studRequest;
	}
	    $date = date('Y-m-d', strtotime($request->today_date));
	    $absent = array();
	    $present = array();
		if(count($results)> 0) {
			for($i =0; $i <count($results);$i++) {
					//echo $StudRequests[$i]['roll_number'];die();
					$rollNumber = $results[$i]['roll_number'];
					$attend = $results[$i]['attend'];
					$studentName = getStudentName($link,$rollNumber);
					$teacherName = getTeacherNameStud($link,$request->teacher_id);
					$subjectName = getSubjectName($link,$request->teacher_id);
					if($attend =='P'){
                        $present[] = $attend;
                    }
                    if($attend =='A'){
                        $absent[] = $attend;
                    }

					$sql = "INSERT INTO stud_attendance SET
					  `teacher_id` ='$request->teacher_id',
					  `branch_name` ='$request->batch_name',
					  `course_name` ='$request->course_name',
					  `today_date` ='$date',
					  `attendance_time` ='$request->current_time',
					  `roll_number` = '$rollNumber',	
					  `student_name` = '$studentName',	
					  `teacher_name` = '$teacherName',	
					  `subject_name` = '$subjectName',	
					  `attend_subject` ='$attend',
					  `created_at`=now()";
					if(mysqli_query($link, $sql)){
						$insertData[] = mysqli_insert_id($link);
					}
			}

				if (count($insertData) > 0) {
                    $p= count($present);
                    $a= count($absent);
					$data['success'] = true;
					$data['message'] = 'Student attendance submitted successfully!!.
                                        Total Present count is ='.$p.'
                                        Absent Count is ='.$a.'';
				} else {
					// if there are no errors, return a message
					$data['success'] = false;
					$data['message'] = 'Error In Form Submission, Please try once Again!';
				}
				echo json_encode( $data );
		}
}

/*
 * Submit Question
 */
/**
 * @param $link
 */
function submitQuestion($link)
{
    // set the timezone first
    $endTime = new DateTime(null, new DateTimezone("Asia/Kolkata"));
    $endTime = $endTime->format('h:i:s A');

    $resObj = array();
    $questionRequests = array();
    $postData = file_get_contents( "php://input" );
    $request = json_decode($postData);
    $questionRequests = json_decode(json_encode($request->sectionDetails), true);
    foreach ($questionRequests as $key => $value) {
		$resObj[] = [
				'sectionId' => $value['sectionId'],
				'questionData' => $value['rawData']
			];
    }
	$date = date('Y-m-d', strtotime($request->today_date));
	$studentName = getStudentName($link,$request->roll_number);
	$teacherName = getTeacherNameStud($link,$request->teacher_id);
	$insertData = array();
	$data['success'] = false;

	if(count($resObj) > 0)
	{
		for($i=0; $i < count($resObj); $i++)
		{
			$sectionId = $resObj[$i]['sectionId'];
			$sectionName = getSectionName($link,$sectionId);
			$questionRes =  getQuestionData($link,$resObj[$i]['questionData']);
			$sql = "INSERT INTO mentoring SET
			  `teacher_id` ='$request->teacher_id',
			  `branch_name` ='$request->branch_name',
			  `course_name` ='$request->course_name',
			  `today_date` ='$date',
			  `start_time` ='$request->start_time',
			  `end_time` ='$endTime',
			  `roll_number` = '$request->roll_number',	
			  `student_name` = '$studentName',	
			  `teacher_name` ='$teacherName',	
			  `section_id`='$sectionId',
			  `section_name` ='$sectionName',
			  `question_data` ='$questionRes',
			  `created_at`=now()";

			if(mysqli_query($link, $sql)){
				$insertData[] = mysqli_insert_id($link);
			}

		}
		if (count($insertData) > 0) {
			$data['success'] = true;
			$data['message'] = 'Data Submitted successfully!';
		} else {
			// if there are no errors, return a message
			$data['success'] = false;
			$data['message'] = 'Error In Form Submission, Please try once Again!';
		}
			echo json_encode($data);
	}
}


/*
 * Get Student Name
 */
/**
 * @param $link
 * @param $id
 * @return mixed
 */
function getStudentName($link, $id)
{
    try {
        $sql = "SELECT CONCAT(firstname,' ',lastname) AS studName,stud_id FROM `student` WHERE roll_number='$id'";
        $getID = mysqli_fetch_assoc( mysqli_query( $link, $sql ) );
        $rowId = $getID['stud_id'];
        if ($rowId > 0) {
            $data = $getID['studName'];
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

/*
 * Get Teacher Name
 */
/**
 * @param $link
 * @param $id
 * @return int
 */
function getTeacherNameStud($link, $id)
{
    $data = 0;
    try {
        $sql = "SELECT CONCAT(firstname,' ',lastname) AS teacherName,id FROM `teacher` WHERE id='$id'";
        $getID = mysqli_fetch_assoc(mysqli_query($link, $sql));
        $rowId = $getID['id'];
        if ($rowId > 0) {
            $data = $getID['teacherName'];
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}


/**
 * @param $link
 * @param $id
 * @return int
 */
function getSubjectName($link, $id)
{
    $data = 0;
    try {
        $sql = "SELECT subject,id FROM `teacher` WHERE id='$id'";
        $getID = mysqli_fetch_assoc(mysqli_query($link, $sql));
        $rowId = $getID['id'];
        if ($rowId > 0) {
            $data = $getID['subject'];
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

/**
 * @param $link
 * @param $id
 * @return int
 */
function getSectionName($link, $id){
	$data = 0;
    try {
        $sql = "SELECT section_name,id FROM `section` WHERE id='$id'";
        $getID = mysqli_fetch_assoc(mysqli_query($link, $sql));
        $rowId = $getID['id'];
        if ($rowId > 0) {
            $data = $getID['section_name'];
        }
        return $data;
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}

/*
*Get Question Data
*/
/**
 * @param $link
 * @param $questionData
 * @return false|string
 */
function getQuestionData($link, $questionData)
{
	$res= array();
	foreach($questionData as $questions) {
		foreach($questions as $question){
			$res[] = $question;
		}
	}
	return json_encode($res,JSON_UNESCAPED_SLASHES);
}

/**
 * @param $link
 * @param $teacherId
 */
function getDashBoardCount($link, $teacherId)
{
    $data = array();
    $sqlLecture = "SELECT COUNT('id') AS countLecture FROM `attendance` WHERE teacher_id='".$teacherId."'";
    $sqlAttendance = "SELECT COUNT('id') AS countAttendance FROM `stud_attendance` WHERE teacher_id='".$teacherId."'";
    $sqlMentor = "SELECT COUNT('id') AS mentoring FROM `mentoring` WHERE teacher_id='".$teacherId."'";

    $rowLecture=mysqli_fetch_array(mysqli_query($link, $sqlLecture));
    $rowAttendance=mysqli_fetch_array(mysqli_query($link, $sqlAttendance));
    $rowMentor=mysqli_fetch_array(mysqli_query($link, $sqlMentor));
    $data['dashBoardCount'] =[
        'lecture' => ($rowLecture[0] >0 ) ? $rowLecture[0] : 0,
        'attendance' => ($rowAttendance[0] >0 ) ? $rowAttendance[0] : 0,
        'mentor' => ($rowMentor[0] >0 ) ? $rowMentor[0] : 0
    ];
    echo json_encode($data);
}

/**
 * @param $link
 * @param $id
 */
function deleteAttendance($link, $id)
{
    $data = array();
    $sql = "UPDATE `attendance` SET status=0 WHERE id='".$id."'";
    if(mysqli_query($link, $sql)) {
        $data['success'] = true;
        $data['message'] = 'Data deleted successfully!';
    }
    echo json_encode($data);
}

/**
 * @param $link
 * @param $edid
 */
function editLecture($link, $edid)
{
    try {
        $data = array();
        $sql = "SELECT * FROM `attendance` WHERE id='$edid'";
        $result = mysqli_query( $link, $sql );
        if ($result) {
            while ($row = mysqli_fetch_assoc( $result )) {
                $data['result'] = $row;
                $data['success'] = true;
            }
        } else {
            $data['success'] = false;
            $data['error'] = 'Error in edit lecture.Please try again!';
        }
        echo json_encode( $data );
    } catch ( Exception $e ) {
        echo $e->getMessage();
    }
}