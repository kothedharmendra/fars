<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

$conn = new mysqli( $servername, $username, $password );
mysqli_select_db( $conn, $dbname );

if (isset( $_POST ) && !empty( $_POST )) {

    if (isset( $_POST['roll_number'] ) && !empty( $_POST['roll_number'] )) {
        $id = $_POST['roll_number'];
        $sql = "SELECT roll_number,student_name,branch_name,today_date,current_time,attend_subject FROM stud_attendance WHERE roll_number='" . $id . "'";
    } elseif(isset( $_POST['from_date'] ) && !empty( $_POST['from_date'] )) {
        $fromDate = date( 'Y-m-d', strtotime( $_POST['from_date'] ) );
        $toDate = date( 'Y-m-d', strtotime( $_POST['to_date'] ) );
        if ($toDate < $toDate) {
            $_SESSION['error'] = "Wrong Date selection!";
            header( 'location: attendance.php' );
            exit();
        }
        $sql = "SELECT roll_number,student_name,branch_name,today_date,current_time,attend_subject FROM stud_attendance
                WHERE today_date between '" . $fromDate . "' AND '" . $toDate . "' ";

        } elseif(isset($_POST['branch_name'] ) && !empty($_POST['branch_name'] )) {
            $batch = trim( $_POST['branch_name'] );
            $sql = "SELECT roll_number,student_name,branch_name,today_date,current_time,attend_subject FROM
                stud_attendance WHERE branch_name='" . $batch . "'";

        }
        $setRec = mysqli_query( $conn, $sql );
        $columnHeader = '';
        $columnHeader = "Sr.No" . "\t" . "Roll Number" . "\t" . "Name of Student " . "\t" . "BATCH" . "\t" . "Date" . "\t" . "Time" . "\t" . "Subject Attendance";
        $setData = '';
        $i = 1;
        while ($rec = mysqli_fetch_row( $setRec )) {
            $rowData = $i . "\t";
            foreach ($rec as $value) {
                $value = '"' . $value . '"' . "\t";
                $rowData .= $value;
            }
            $i++;
            $setData .= trim( $rowData ) . "\n";
        }

        header( "Content-Type: application/xls" );
        header( 'Content-Disposition: attachment; filename=Academic_Progress.xls' );
        header( "Pragma: no-cache" );
        header( "Expires: 0" );
        echo ucwords( $columnHeader ) . "\n" . $setData . "\n";
    }


