<?php

// Turn off all error reporting
error_reporting(1);

require_once '../config/config.php';

$conn = mysqli_connect($servername, $username, $password,$dbname);
if(!$conn) {
    echo mysqli_error( $conn ); die();
}

$fromDate = date('Y-m-d');
$toDate = date('Y-m-d');

$sql = "SELECT a.date, a.batch, CONCAT(b.title,' ',b.firstname,' ',b.lastname) AS fname,a.subject ,a.branch,a.sequence_no,
            c.topic_name,a.present_head_count,a.doubt_clearing_count,a.mentoring_count,a.start_time,a.end_time,a.lecture_no,a.total_lecture_no,a.balence_lecture
	    FROM `attendance` AS a
        INNER JOIN teacher AS b ON a.teacher_id=b.id 
        INNER JOIN course as c ON c.id= a.topic_taught WHERE a.date between '".$fromDate."' AND '".$toDate."' ";

$setRec = mysqli_query($conn, $sql);
$setData = '';
$setData = "Sr.No". "\t" ."DATE" . "\t" . "BATCHES" . "\t" . "FACULTY" . "\t". "SUBJECT". "\t" . "COURSE". "\t". "SEQUENCE NO". "\t" . "TAUGHT TOPIC". "\t" . "PRESENT HEAD COUNT". "\t" . "DOUBT CLEARING COUNT". "\t" . "MENTORING COUNT"
    . "\t" . "START TIME". "\t" . "END TIME". "\t" ."LECTURE NO". "\t" ."TOTAL NUMBER OF LECTURE". "\t"."BALENCE LECTURE \n";
$i =1;
if(mysqli_fetch_row($setRec)!=''){
    while ($rec = mysqli_fetch_row($setRec)) {
        $rowData = $i . "\t";
        foreach ($rec as $value) {
            $value = '"' . $value . '"' . "\t";
            $rowData .= $value;
        }
        $i++;
        $setData .= trim($rowData) . "\n";
    }
    $csv_handler = fopen ('Academic_Progress_'.$fromDate.'.xls','w');
    fwrite ($csv_handler,$setData);
    fclose ($csv_handler);


$filename = 'Academic_Progress_'.$fromDate.'.xls';
$path = realpath($filename);
$file = $path;

$mailto = 'dk@abstractitgroup.com';
$subject = 'Academic Progress Report';
$message .= 'Hello Team'."\n";
$message .=  'Please find attached daily report of Academic Progress';

$content = file_get_contents($file);
$content = chunk_split(base64_encode($content));

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (RFC)
$eol = "\r\n";

// main header (multipart mandatory)
$headers = "From: Fars <dk@abstractitgroup.com>" . $eol;
$headers .= "MIME-Version: 1.0" . $eol;
$headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
$headers .= "Content-Transfer-Encoding: 7bit" . $eol;
$headers .= "This is a MIME encoded message." . $eol;

// message
$body = "--" . $separator . $eol;
$body .= "Content-Type: text/plain; charset=\"iso-8859-1\"" . $eol;
$body .= "Content-Transfer-Encoding: 8bit" . $eol;
$body .= $message . $eol;

// attachment
$body .= "--" . $separator . $eol;
$body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
$body .= "Content-Transfer-Encoding: base64" . $eol;
$body .= "Content-Disposition: attachment" . $eol;
$body .= $content . $eol;
$body .= "--" . $separator . "--";

    //SEND Mail
    if (mail($mailto, $subject, $body, $headers)) {
        echo "mail send ... OK"; // or use booleans here
        unlink($file);
    } else {
        echo "mail send ... ERROR!";
        print_r( error_get_last() );
    }
    echo 'Data saved to Academic_Progress_'.$fromDate.'.xls';
} else {
    echo "No Record Found";
}
?>
