<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

$conn = new mysqli( $servername, $username, $password );
mysqli_select_db( $conn, $dbname );

if (isset( $_POST ) && !empty( $_POST )) {

    if (isset( $_POST['from_date'] ) && !empty( $_POST['from_date'] )) {
        $fromDate = date( 'Y-m-d', strtotime( $_POST['from_date'] ) );
        $toDate = date( 'Y-m-d', strtotime( $_POST['to_date'] ) );

        if ($toDate < $toDate) {
            $_SESSION['error'] = "Wrong Date selection!";
            header( 'location: attendance.php' );
            exit();
        }

		$select="select * from admin_accounts where id=".$_SESSION['user_id'];
$menu = mysqli_query( $conn,$select );
$row=mysqli_fetch_array($menu);
$member_type=$row["member_type"];
$cond="";
if($member_type=="U")
{
  $arr=explode(",", $row['batches']);
$str="";
for($i=0;$i<count($arr);$i++)
{
	if($str=="")
	   $str="'".$arr[$i]."'";
	else
		$str.=",'".$arr[$i]."'";
}
$cond=" and branch in (".$str.")"; 
}

       $sql = "SELECT a.date, a.batch, CONCAT(b.title,' ',b.firstname,' ',b.lastname) AS fname,a.subject , a.sequence_no,c.topic_name,a.present_head_count,a.doubt_clearing_count,a.mentoring_count,a.start_time,a.end_time,
		a.lecture_no,a.total_lecture_no,a.balence_lecture,date_format(a.created_at,'%h:%i %a') as submission_date
		FROM `attendance` AS a INNER JOIN teacher AS b ON a.teacher_id=b.id 
        INNER JOIN course as c ON c.id= a.topic_taught WHERE a.date between '" . $fromDate . "' AND '" . $toDate . "' ".$cond;
    } elseif (isset( $_POST['teacher_export'] ) && !empty( $_POST['teacher_export'] )) {
        $id  = $_POST['teacher_export'];
        $sql = "SELECT a.date, a.batch, CONCAT(b.title,' ',b.firstname,' ',b.lastname) AS fname,a.subject , a.sequence_no,c.topic_name,a.present_head_count,a.doubt_clearing_count,a.mentoring_count,a.start_time,a.end_time,
		a.lecture_no,a.total_lecture_no,a.balence_lecture,date_format(a.created_at,'%h:%i %a') as submission_date
		FROM `attendance` AS a INNER JOIN teacher AS b ON a.teacher_id=b.id 
        INNER JOIN course as c ON c.id= a.topic_taught WHERE b.id='".$id."'";
    } elseif (isset( $_POST['branch_name'] ) && !empty( $_POST['branch_name'] )) {
        $batch = $_POST['branch_name'];
        $sql = "SELECT a.date, a.batch, CONCAT(b.title,' ',b.firstname,' ',b.lastname) AS fname,a.subject , a.sequence_no,c.topic_name,a.present_head_count,a.doubt_clearing_count,a.mentoring_count,a.start_time,a.end_time,
		a.lecture_no,a.total_lecture_no,a.balence_lecture,date_format(a.created_at,'%h:%i %p') as submission_date
		FROM `attendance` AS a INNER JOIN teacher AS b ON a.teacher_id=b.id 
        INNER JOIN course as c ON c.id= a.topic_taught WHERE a.batch='".trim($batch)."'";
    }

    $setRec = mysqli_query( $conn, $sql );
    $columnHeader = '';
    $columnHeader = "Sr.No" . "\t" . "DATE" . "\t" . "BATCHES" . "\t" . "FACULTY" . "\t" . "SUBJECT" . "\t" . "SEQUENCE NO" . "\t" . "TAUGHT TOPIC" . "\t" . "PRESENT HEAD COUNT" . "\t" . "DOUBT CLEARING COUNT" . "\t" . "MENTORING COUNT"
        . "\t" . "START TIME" . "\t" . "END TIME" . "\t" . "LECTURE NO" . "\t" . "TOTAL NUMBER OF LECTURE" . "\t" . "BALENCE LECTURE". "\t" . "Report Submission Time";
    $setData = '';
    $i = 1;
    while ($rec = mysqli_fetch_row( $setRec )) {
        $rowData = $i . "\t";
        foreach ($rec as $value) {
            $value = '"' . $value . '"' . "\t";
            $rowData .= $value;
        }
        $i++;
        $setData .= trim( $rowData ) . "\n";
    }

    header( "Content-Type: application/xls" );
    header( 'Content-Disposition: attachment; filename=Academic_Progress.xls' );
    header( "Pragma: no-cache" );
    header( "Expires: 0" );
    echo ucwords( $columnHeader ) . "\n" . $setData . "\n";
}


