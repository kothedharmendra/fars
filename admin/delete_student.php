<?php 
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$del_id = filter_input(INPUT_POST, 'del_id');
if ($del_id && $_SERVER['REQUEST_METHOD'] == 'POST') 
{
    $customer_id = $del_id;
    $db->where('stud_id', $customer_id);
	foreach($data_to_update as $key=>$value)
    {
        if(is_null($value) || $value == '')
            unset($data_to_update[$key]);
    }
    
    $status = $db->update('student', $data_to_update);
	
    if ($status)
    {
        $_SESSION['info'] = "Student deleted successfully!";
        header('location: student.php');
        exit;
    }
    else
    {
    	$_SESSION['failure'] = "Unable to delete student";
    	header('location: studen.php');
        exit;

    }
}
