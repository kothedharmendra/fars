<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

$conn = new mysqli($servername, $username, $password );
mysqli_select_db($conn, $dbname);

if(isset($_POST) && !empty($_POST)){
	$id  = $_POST['teacher_export'];
	$batch = $_POST['batch'];
	if(isset($_POST['batch']) && !empty($_POST['batch']))
    {
       $sql = "SELECT a.date, a.batch,a.subject,a.sequence_no,c.topic_name, a.lecture_no,a.total_lecture_no,a.balence_lecture
		FROM `attendance` AS a 
		INNER JOIN teacher AS b ON a.teacher_id=b.id 
        INNER JOIN course as c ON c.id= a.topic_taught WHERE a.batch='".trim($batch)."'";
    }else{
        $sql = "SELECT a.date, a.batch,a.subject,a.sequence_no,c.topic_name, a.lecture_no,a.total_lecture_no,a.balence_lecture
		FROM `attendance` AS a 
		INNER JOIN teacher AS b ON a.teacher_id=b.id 
        INNER JOIN course as c ON c.id= a.topic_taught WHERE b.id='".$id."'";
    }

    $setRec = mysqli_query($conn, $sql);
    $columnHeader = '';
    $columnHeader = "Sr.No". "\t" ."DATE" . "\t" . "BATCH " ."\t". "SUBJECT". "\t". "SEQUENCE NO". "\t" . "TAUGHT TOPIC". "\t" . "LECTURE NO". "\t" . "TOTAL NUMBER OF LECTURE". "\t" ."BALENCE LECTURE";
    $setData = '';
    $i =1;
    while ($rec = mysqli_fetch_row($setRec)) {
        $rowData = $i . "\t";
        foreach ($rec as $value) {
            $value = '"' . $value . '"' . "\t";
            $rowData .= $value;
        }
        $i++;
        $setData .= trim($rowData) . "\n";
    }

    header("Content-Type: application/xls");
    header('Content-Disposition: attachment; filename=Academic_Progress.xls');
    header("Pragma: no-cache");
    header("Expires: 0");
    echo ucwords($columnHeader) . "\n" . $setData . "\n";
}


