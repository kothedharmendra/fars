<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

// Sanitize if you want
$section_id = filter_input(INPUT_GET, 'section_id', FILTER_VALIDATE_INT);
$operation = filter_input(INPUT_GET, 'operation',FILTER_SANITIZE_STRING); 
($operation == 'edit') ? $edit = true : $edit = false;

//Handle update request. As the form's action attribute is set to the same script, but 'POST' method, 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Get customer id form query string parameter.
    $section_id = filter_input(INPUT_GET, 'section_id', FILTER_SANITIZE_STRING);
    //Get input data
    $data_to_update = filter_input_array(INPUT_POST);
    $data_to_update['updated_at'] = date('Y-m-d H:i:s');
    $db->where('id',$section_id);
    $data_to_update['section_name'] = $_POST['section_name'];
    $data_to_update['master_question_id'] = implode( ',',$data_to_update['master_question_id']);
    $data_to_update['status'] = $_POST['status'];
    $stat = $db->update('section', $data_to_update);
    if($stat)
    {
        $_SESSION['success'] = "Section updated successfully!";
        //Redirect to the listing page,
        header('location: section.php');
        //Important! Don't execute the rest put the exit/die. 
        exit();
    }
}

//If edit variable is set, we are performing the update operation.
if($edit)
{
    $db->where('id', $section_id);
    //Get data to pre-populate the form.
    $section = $db->getOne("section");
}
?>
<?php include_once 'includes/header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <h2 class="page-header">Update Section</h2>
    </div>
    <!-- Flash messages -->
    <?php
        include('./includes/flash_messages.php')
    ?>
    <form class="" action="" method="post" enctype="multipart/form-data" id="teacher_form">
        <fieldset>
    <div class="form-group">
        <label for="topic_name">Section Name *</label>
        <input type="text" name="section_name" value="<?php echo $edit ? $section['section_name'] : ''; ?>"
               placeholder="Enter Section Name" class="form-control" required="required" id="section_name">
    </div>
            <?php
            $query = "SELECT id,question_name FROM question WHERE status=1";
            $selectedBatch= $db->rawQuery($query);
            $usersCourse = explode( ",", $section['master_question_id'] );
            ?>
            <div class="form-group">
                <label>All Question</label>
                <select name="master_question_id[]" class="form-control selectpicker" required="required" multiple id="master_question_id">
                    <?php
                    foreach ($selectedBatch as $value) {
                        if ($edit && in_array( $value['id'], $usersCourse)) {
                            $sel = "selected";
                        } else {
                            $sel = "";
                        }
                        echo '<option value="' . $value['id'] . '"' . $sel . '>' . $value['id'].'. '. $value['question_name'] . '</option>';
                    }
                    ?>
                </select>
            </div>
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $section['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $("#section_form").validate({
       rules: {
           section_name: {
                required: true,
                minlength: 3
            },   
        }
    });
});
</script>
<?php include_once 'includes/footer.php'; ?>