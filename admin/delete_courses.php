<?php 
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$del_id = filter_input(INPUT_POST, 'del_id');
if ($del_id && $_SERVER['REQUEST_METHOD'] == 'POST') 
{
    $customer_id = $del_id;
    $db->where('id', $customer_id);
    $status = $db->delete('course_master');
    if ($status)
    {
        $_SESSION['info'] = "Course deleted successfully!";
        header('location: courses.php');
        exit;
    }
    else
    {
    	$_SESSION['failure'] = "Unable to delete course";
    	header('location: courses.php');
        exit;

    }
}
