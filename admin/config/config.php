<?php 

define('BASE_PATH', dirname(dirname(__FILE__)));
define('APP_FOLDER','simpleadmin');

// Turn off all error reporting
error_reporting(-1);
ini_set('memory_limit', '-1'); 

require_once BASE_PATH.'/lib/MysqliDb.php';
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "app_fars";
// create connection object

$db = new MysqliDb($servername,$username,$password,$dbname);