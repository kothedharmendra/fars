<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to section.php page.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //print_r($_POST);die();
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $data_to_store = filter_input_array( INPUT_POST );
    //Insert timestamp
    $data_to_store['created_at'] = date( 'Y-m-d H:i:s' );
    $data_to_store['master_question_id'] = implode( ',',$data_to_store['master_question_id']);


    $data_to_store['section_name'] = $_POST['section_name'];
    $data_to_store['status'] = $_POST['status'];

    $last_id = $db->insert('section', $data_to_store);
    if ($last_id) {
        $_SESSION['success'] = "Section added successfully!";
        header( 'location: section.php' );
        exit();
    }
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

require_once 'includes/header.php';
?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Add Section</h2>
            </div>
        </div>
        <form class="form" action="" method="post" id="section_form" enctype="multipart/form-data">
            <fieldset>
                <div class="form-group">
                    <label for="topic_name">Section Name *</label>
                    <input type="text" name="section_name" value="<?php echo $edit ? $section['section_name'] : ''; ?>"
                           placeholder="Enter Section Name" class="form-control" required="required" id="section_name">
                </div>
                <?php
                $query = "SELECT id,question_name FROM question WHERE status=1";
                $selectedBatch= $db->rawQuery($query);
                if($edit)
                {
                    $usersCourse = explode( ",", $section['master_question_id'] );

                }
                ?>
                <div class="form-group">
                    <label>All Question</label>
                    <select name="master_question_id[]" class="form-control selectpicker" required="required" multiple id="master_question_id">
                        <?php
                        foreach ($selectedBatch as $value) {
                            if ($edit && in_array( $value['id'], $usersCourse)) {
                                $sel = "selected";
                            } else {
                                $sel = "";
                            }
                            echo '<option value="' . $value['id'] . '"' . $sel . '>' .$value['id'].'. '. $value['question_name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
                    <select name="status" class="form-control selectpicker" required>
                        <?php
                        foreach ($opt_arr as $opt => $value) {
                            if ($edit && $opt == $section['status']) {
                                $sel = "selected";
                            } else {
                                $sel = "";
                            }
                            echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group text-center">
                    <label></label>
                    <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span>
                    </button>
                </div>
            </fieldset>
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#section_form").validate({
                rules: {
                    section_name: {
                        required: true,
                        minlength: 3
                    },
                }
            });
        });
    </script>
<?php include_once 'includes/footer.php'; ?>