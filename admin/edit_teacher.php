<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

// Sanitize if you want
$customer_id = filter_input(INPUT_GET, 'teacher_id', FILTER_VALIDATE_INT);
$operation = filter_input(INPUT_GET, 'operation',FILTER_SANITIZE_STRING); 
($operation == 'edit') ? $edit = true : $edit = false;

//Handle update request. As the form's action attribute is set to the same script, but 'POST' method, 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Get customer id form query string parameter.
    $customer_id = filter_input(INPUT_GET, 'teacher_id', FILTER_SANITIZE_STRING);
    //Get input data
    $data_to_update = filter_input_array(INPUT_POST);
    $data_to_update['updated_at'] = date('Y-m-d H:i:s');
    $db->where('id',$customer_id);

    if(!empty($data_to_update['password']) && $data_to_update['password']!=''){
        $data_to_update['password'] = md5($data_to_update['password']);
    }
    $data_to_update['branch_name'] = implode( ',',$data_to_update['branch_name']);
    $data_to_update['course_name'] = implode( ',',$data_to_update['course_name']);
    $data_to_update['subject'] = $data_to_update['subject'];

    foreach($data_to_update as $key=>$value)
    {
        if(is_null($value) || $value == '')
            unset($data_to_update[$key]);
    }
    
    //implode( ',',$data_to_update['subject']);
    //$data_to_update['topic_name'] = implode( ',',$data_to_update['topic_name']);
    $stat = $db->update('teacher', $data_to_update);
    if($stat)
    {
        $_SESSION['success'] = "Teacher updated successfully!";
        //Redirect to the listing page,
        header('location: teacher.php');
        //Important! Don't execute the rest put the exit/die. 
        exit();
    }
}

//If edit variable is set, we are performing the update operation.
if($edit)
{
    $db->where('id', $customer_id);
    //Get data to pre-populate the form.
    $customer = $db->getOne("teacher");
    //$usersselectedCourse = explode( ",", $customer['course_name'] );
}
?>
<?php include_once 'includes/header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <h2 class="page-header">Update Teacher</h2>
    </div>
    <!-- Flash messages -->
    <?php
        include('./includes/flash_messages.php')
    ?>
    <form class="" action="" method="post" enctype="multipart/form-data" id="teacher_form">
        <?php
            //Include the common form for add and edit  
            require_once('./includes/forms/teacher_form.php');
        ?>
    </form>
</div>
<script type="text/javascript">
function displayBatch(sel) {
    var coursename = $('#course_name').val();
    var selectedbatch='<?php echo $customer["branch_name"]?>';
    if (coursename.length > 0 ) {

     $.ajax({
            type: "POST",
            url: "fetch_batch.php",
            data: "coursename="+coursename+"&selectedbatch="+selectedbatch,
            cache: false,
            beforeSend: function () { 
                //$('#output1').html('<img src="loader.gif" alt="" width="24" height="24">');
            },
            success: function(html) { 
            
                $("#branch_name").html( html );
            }
        });
    } 
}
</script>
<?php include_once 'includes/footer.php'; ?>