<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

// Sanitize if you want
$customer_id = filter_input(INPUT_GET, 'course_id', FILTER_VALIDATE_INT);
$operation = filter_input(INPUT_GET, 'operation',FILTER_SANITIZE_STRING); 
($operation == 'edit') ? $edit = true : $edit = false;

//Handle update request. As the form's action attribute is set to the same script, but 'POST' method, 
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Get customer id form query string parameter.
    $customer_id = filter_input(INPUT_GET, 'course_id', FILTER_SANITIZE_STRING);
    //Get input data
    $data_to_update = filter_input_array(INPUT_POST);
    $data_to_update['updated_at'] = date('Y-m-d H:i:s');
    $db->where('id',$customer_id);
    $data_to_update['course_name'] = $_POST['course_name'];
    $data_to_update['status'] = $_POST['status'];
    //$data_to_update['topic_name'] = implode( ',',$data_to_update['topic_name']);
    $stat = $db->update('course_master', $data_to_update);
    if($stat)
    {
        $_SESSION['success'] = "Course updated successfully!";
        //Redirect to the listing page,
        header('location: courses.php');
        //Important! Don't execute the rest put the exit/die. 
        exit();
    }
}

//If edit variable is set, we are performing the update operation.
if($edit)
{
    $db->where('id', $customer_id);
    //Get data to pre-populate the form.
    $customer = $db->getOne("course_master");
}
?>
<?php include_once 'includes/header.php'; ?>
<div id="page-wrapper">
    <div class="row">
        <h2 class="page-header">Update Course</h2>
    </div>
    <!-- Flash messages -->
    <?php
        include('./includes/flash_messages.php')
    ?>
    <form class="" action="" method="post" enctype="multipart/form-data" id="teacher_form">
        <fieldset>
    <div class="form-group">
        <label for="topic_name">Course Name *</label>
        <input type="text" name="course_name" value="<?php echo $edit ? $customer['course_name'] : ''; ?>"
               placeholder="Enter Course Name" class="form-control" required="required" id="course_name">
    </div>
    
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $customer['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $("#teacher_form").validate({
       rules: {
           course_name: {
                required: true,
                minlength: 3
            },   
        }
    });
});
</script>
<?php include_once 'includes/footer.php'; ?>