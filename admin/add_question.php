<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to question.php page.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //print_r($_POST);die();
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $data_to_store = filter_input_array( INPUT_POST );
    //Insert timestamp
    $data_to_store['created_at'] = date( 'Y-m-d H:i:s' );

    $data_to_store['question_name'] = $_POST['question_name'];
    $data_to_store['status'] = $_POST['status'];

    $last_id = $db->insert('question', $data_to_store);
    if ($last_id) {
        $_SESSION['success'] = "Question added successfully!";
        header( 'location: question.php' );
        exit();
    }
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

require_once 'includes/header.php';
?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Add Question</h2>
            </div>
        </div>
        <form class="form" action="" method="post" id="question_form" enctype="multipart/form-data">
            <fieldset>
                <div class="form-group">
                    <label for="topic_name">Question *</label>
                    <textarea name="question_name" placeholder="Enter Question" class="form-control" required="required"
                              id="question_name"><?php echo $edit ? $question['question_name'] : ''; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
                    <select name="status" class="form-control selectpicker" required>
                        <?php
                        foreach ($opt_arr as $opt => $value) {
                            if ($edit && $opt == $question['status']) {
                                $sel = "selected";
                            } else {
                                $sel = "";
                            }
                            echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group text-center">
                    <label></label>
                    <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span>
                    </button>
                </div>
            </fieldset>
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#question_form").validate({
                rules: {
                    question_name: {
                        required: true,
                        minlength: 3
                    },
                }
            });
        });
    </script>
<?php include_once 'includes/footer.php'; ?>