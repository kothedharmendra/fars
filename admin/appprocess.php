<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST");
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

require_once '../config/config.php';
$link =  mysqli_connect($servername,$username,$password,$dbname);
if(!$link) {
    die(mysqli_error($link));
}
if (isset( $_GET["opt"] ) && !empty( $_GET["opt"] )) {
    $opt = $_GET["opt"];
} else {
    $opt = "";
}

if(isset($_GET["sub"]) && !empty( $_GET["sub"] )){
	$sub = ucwords($_GET["sub"]);
}

if(isset($_GET["teacher_id"]) && !empty( $_GET["teacher_id"] )){
	$teacherId = $_GET["teacher_id"];
}

if(isset($_GET["list"]) && !empty( $_GET["list"] )){
	$list = $_GET["list"];
}

if(isset($_GET["tid"]) && !empty($_GET["tid"])){
	$taughtId = $_GET["tid"];
}

if(isset($_GET["course"]) && !empty($_GET["course"])){
	$course = $_GET["course"];
}

switch ($opt) {
    case("login"):
        $res = loginAction($link);
        print_r($res);
        break;
    case("add"):
        $res = addAction( $link );
        print_r( $res );
        break;
        
    case("topic"):
        $res = getTopicAction( $link, $course,$sub );
        print_r($res);
        break;
		
	case("lecture"):
        $res = getTopicNumber( $link, $taughtId);
        print_r($res);
        break;
        
    case("listview"):
        $res = getViewAction( $link, $teacherId, $list );
        print_r($res);
        break;

    default:
}

/**
 * @param $link
 */
function addAction($link)
{
    $postData = file_get_contents("php://input");
    $request = json_decode($postData);
    $teacherId = $request->teacher_id;
    $batch = $request->batch;
    $date = $request->date;
    $subject = $request->subject;
    $topicTaught = $request->topic_taught;
    $presentHeadCount = $request->present_head_count;
    $doubtClearingCount = $request->doubt_clearing_count;
    $mentoringCount = $request->mentoring_count;
    $startTime = $request->start_time;
    $endTime = $request->end_time;
    $lectureNo = $request->lecture_no;
    $totalLectureNo = $request->total_lecture_no;
    $sequenceNo = $request->sequence_no;

    //INSERT STUDENT INFORMATION
    $sql = "INSERT INTO attendance SET
          `teacher_id` ='$teacherId',`batch` ='$batch',
          `date` ='$date',
		  `subject` ='$subject',
		  `topic_taught` ='$topicTaught',
          `present_head_count` ='$presentHeadCount',
          `doubt_clearing_count` ='$doubtClearingCount',
          `mentoring_count` ='$mentoringCount',
		  `sequence_no` ='$sequenceNo',
		  `total_lecture_no` ='$totalLectureNo',
          `start_time` ='$startTime',`end_time` ='$endTime',
          `lecture_no` ='$lectureNo',`created_at`=now() ";
    $result = mysqli_query( $link, $sql );
    if ($result) {
        $data['success'] = true;
        $data['message'] = 'Data Inserted successfully!';
    } else {
        // if there are no errors, return a message
        $data['success'] = false;
        $data['message'] = 'Error In Form Submission, Please try once Again!';
    }
    echo json_encode($data);
}

/**
 * @param $link
 */
function loginAction($link)
{
    $postData = file_get_contents("php://input");
    $request = json_decode($postData);
	if($request){
		
    $email = strtolower(trim($request->id));
    $password = md5(trim($request->password));
    $sql = "SELECT * FROM teacher WHERE `email`='$email'
    AND `password`='$password' AND `status`='1'";
    $getID = mysqli_fetch_assoc(mysqli_query($link, $sql));
    $rowId = $getID['id'];
    if ($rowId > 0) {
        $data['success'] = true;
        $data['name'] = ucfirst($getID['firstname']." ".$getID['lastname']);
        $data['teacher_id'] = $rowId;
        $data['allow_pastdate'] = $getID['allow_pastdate'];
        $data['past_date'] = '6';
        $data['branch_name'] = explode(',',$getID['branch_name']);
        $data['course_name'] = $getID['course_name'];
        $data['subject'] = explode(',',$getID['subject']);
        $data['message'] = 'Login successfully!';
    } else {
        // if there are no errors, return a message
        $data['success'] = false;
        $data['message'] = 'Incorrect Username/Password';
    }
		echo json_encode($data);
	}
}

function getTopicAction($link, $course,$sub){
	$listArray = array_map('trim', explode(',', $course));
	$listWhere = "'".implode("','", $listArray)."'";
    $data = array();
   $sql = "SELECT topic_name,id FROM course WHERE `course_year` IN($listWhere) AND subject='".$sub."' AND `status`='1'";
    $result = mysqli_query( $link, $sql);
   if ($result) {
       while ($row = mysqli_fetch_assoc($result)) {
          $data[] = $row;
       }
    } else {
        // if there are no errors, return a message
        $data["topic"]  = 'No Topic For this Subject';
    }
    echo json_encode($data);
}

//View Report
function getViewAction($link, $teacherId, $list)
 {
	try{
		$data = array();
		$sql = "SELECT * FROM attendance WHERE `teacher_id`='".$teacherId."' AND `date`='".$list."'";
		$result = mysqli_query( $link, $sql);
		   if ($result) {
			   while ($row = mysqli_fetch_assoc($result)) {
				  $data[] = $row;
			   }
			} else {
				$data['error']  = 'No attendance Submitted!';
			}
		echo json_encode($data);
	} catch(Exception $e) {
		echo $e->getMessage();
	}
}

//Get Topic Number
function getTopicNumber($link, $id)
{
	try{
		$sql = "SELECT no_lecture, id FROM course WHERE `id`='$id'";
		$getID = mysqli_fetch_assoc(mysqli_query($link, $sql));	
		$rowId = $getID['id'];
		if ($rowId > 0) {
			$data['no_lecture'] = $getID['no_lecture'];
		} else {
			// if there are no errors, return a message
			$data['no_lecture'] = '0';
		}
			echo json_encode($data);
	} catch(Exception $e) {
		echo $e->getMessage();
	}
}