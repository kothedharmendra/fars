<fieldset>
    <div class="form-group">
        <label for="name"> First Name *</label>
        <input type="text" name="firstname" value="<?php echo $edit ? $student['firstname'] : ''; ?>"
               placeholder="First Name" class="form-control" required="required" id="firstname">
    </div>
    <div class="form-group">
        <label for="middlename"> Middle Name *</label>
        <input type="text" name="middlename" value="<?php echo $edit ? $student['middlename'] : ''; ?>"
               placeholder="Middle Name" class="form-control" required="required" id="middlename">
    </div>
    <div class="form-group">
        <label for="lastname"> Last Name *</label>
        <input type="text" name="lastname" value="<?php echo $edit ? $student['lastname'] : ''; ?>"
               placeholder="Last Name" class="form-control" required="required" id="lastname">
    </div>
    <div class="form-group">
        <label for="roll_number">Roll Number *</label>
        <input type="text" name="roll_number" value="<?php echo $edit ? $student['roll_number'] : ''; ?>"
               placeholder="Roll Number" class="form-control" required="required" id="roll_number">
    </div>
    <div class="form-group">
        <label for="email">Email *</label>
        <input type="email" name="email" value="<?php echo $edit ? $student['email'] : ''; ?>"
               placeholder="E-Mail Address" class="form-control" id="email" required="required">
    </div>
    <div class="form-group">
        <label for="contact_number">Contact Number *</label>
        <input type="text" name="contact_number" value="<?php echo $edit ? $student['contact_number'] : ''; ?>"
               placeholder="Contact Number" class="form-control" required="required" id="contact_number">
    </div>
    <div class="form-group">
        <label for="parent_contact_number">Parent Contact Number *</label>
        <input name="parent_contact_number" value="<?php echo $edit ? $student['parent_contact_number'] : ''; ?>"
               placeholder="9890267215"
               class="form-control" type="text" id="parent_contact_number" required="required"/>
    </div>
    <div class="form-group">
        <label for="address">Address *</label>
        <textarea name="address" value=""
                  placeholder="Address" class="form-control" required="required"
                  id="address"><?php echo $edit ? $student['adhar_number'] : ''; ?></textarea>
    </div>
    <div class="form-group">
        <label for="category">Category *</label>
        <input name="category" value="<?php echo $edit ? $student['category'] : ''; ?>"
               class="form-control" type="text" id="category" required="required"/>
    </div>
    <div class="form-group">
        <label for="adhar_number">AAdhar Number *</label>
        <input name="adhar_number" value="<?php echo $edit ? $student['adhar_number'] : ''; ?>"
               placeholder="1234-12345-1234"
               class="form-control" type="text" id="adhar_number" required="required"/>
    </div>
    <?php
    $query = "SELECT branch_name FROM batch WHERE status=1";
    $selectedBatch= $db->rawQuery($query);
    ?>
    <div class="form-group">
        <label>Batches</label>
        <select name="branch_name" class="form-control selectpicker" required="required">
            <?php
                foreach ($selectedBatch as $key=> $value) {
                    if ($edit && $value['branch_name']==trim($student['branch_name'])) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo '<option value="' . $value['branch_name'] . '"' . $sel . '>' . $value['branch_name'] . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $student['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>


