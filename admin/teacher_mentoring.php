<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

//Get Input data from query string
$search_string = filter_input(INPUT_GET, 'search_string');
$filter_col = filter_input(INPUT_GET, 'filter_col');
$order_by = filter_input(INPUT_GET, 'order_by');
//Get current page.
$page = filter_input(INPUT_GET, 'page');
//Per page limit for pagination.
$pagelimit = 20;
if (!$page) {
    $page = 1;
}
// If filter types are not selected we show latest added data first
if (!$filter_col) {
    $filter_col = "created_at";
}
if (!$order_by) {
    $order_by = "Desc";
}

// select the columns
$select = array('id', 'title','firstname','lastname','subject','status','branch_name');
$db->where('is_delete','0');


//Start building query according to input parameters.
// If search string
if ($search_string) 
{
    $db->where('firstname', '%' . $search_string . '%', 'like');
    $db->orwhere('lastname', '%' . $search_string . '%', 'like');
    $db->orwhere('location', '%' . $search_string . '%', 'like');
    $db->orwhere('phone', '%' . $search_string . '%', 'like');
    $db->orwhere('status', '%' . $search_string . '%', 'like');
}
    $db->where('is_delete','0');


//Set pagination limit
$db->pageLimit = $pagelimit;

//Get result of the query.
$teacher = $db->arraybuilder()->paginate("teacher", $page, $select);
//print_r($teacher);
$total_pages = $db->totalPages;

// get columns for order filter
foreach ($teacher as $value) {
    foreach ($value as $col_name => $col_value) {
        $filter_options[$col_name] = $col_name;
    }
    //execute only once
    break;
}
include_once 'includes/header.php'; ?>
<!--Main container start-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h1 class="page-header">Mentoring Session</h1>
        </div>
        
    </div>
        <?php include('./includes/flash_messages.php') ?>
    <!--    Begin filter section-->
    <div class="well text-center filter-form">
        <form class="form form-inline" action="">
            <label for="input_search">Search</label>
            <input type="text" class="form-control" id="input_search" name="search_string" value="<?php echo $search_string; ?>">
            <!--<label for ="input_order">Order By</label>
            <select name="filter_col" class="form-control">
                <?php
                foreach ($filter_options as $option) {
                    ($filter_col === $option) ? $selected = "selected" : $selected = "";
                    echo ' <option value="' . $option . '" ' . $selected . '>' . $option . '</option>';
                }
                ?>
            </select>
            <select name="order_by" class="form-control" id="input_order">
                <option value="Asc" <?php
                if ($order_by == 'Asc') {
                    echo "selected";
                }
                ?> >Asc</option>
                <option value="Desc" <?php
                if ($order_by == 'Desc') {
                    echo "selected";
                }
                ?>>Desc</option>
            </select>-->
            <input type="submit" value="Go" class="btn btn-primary">
        </form>
    </div>
<!--   Filter section end-->
    <hr />
    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
             <!--    <th class="header">#</th> -->
				<th>Code</th>
                <th>Title</th>
                <th>First Name</th>
                <th>Last Name</th>
               <th>Subject</th>
			   <th>Batch</th>
                <th>Total Mentoring Session</th>
                
            </tr>
        </thead>
        <tbody>
            <?php
			$link = mysqli_connect( $servername, $username, $password, $dbname );
if (!$link) {
    die( mysqli_error() );
}


            foreach ($teacher as $row) { 
				$mcount=0;
				$sql_m="SELECT roll_number,today_date,start_time,end_time FROM `mentoring` as a, section as b WHERE a.section_id=b.id and teacher_id=".$row["id"]." group by start_time, today_date";
				$res_m=mysqli_query($link,$sql_m);
				$mcount=mysqli_num_rows($res_m);

				

				?>
                <tr>              
					<td><?php echo substr($row['subject'],0,1)."".$row['id']; ?> </td>
	                <td><?php echo $row['title']; ?></td>
	                <td><?php echo $row['firstname'] ?></td>
	                <td><?php echo $row['lastname'] ?> </td>
					<td><?php echo $row['subject'] ?> </td>
					<td><?php 
				$batch_arr=explode(",",$row['branch_name']);	
				for($i=0;$i<count($batch_arr);$i++)
				{
                          $sql_m1="SELECT roll_number,today_date FROM `mentoring` as a, section as b WHERE a.section_id=b.id and teacher_id=".$row["id"]." and branch_name='".$batch_arr[$i]."' group by start_time, today_date";
				$res_m1=mysqli_query($link,$sql_m1);
				$mcount1=mysqli_num_rows($res_m1);
                echo $batch_arr[$i]. " (".$mcount1.")";
				echo "<br>";
				}
				 ?> </td>
	                <td><?php echo $mcount; ?> </td>
	                
				</tr>
            		<!-- Delete Confirmation Modal-->
					 <div class="modal fade" id="confirm-delete-<?php echo $row['id'] ?>" role="dialog">
					    <div class="modal-dialog">
					      <form action="delete_teacher.php" method="POST">
					      <!-- Modal content-->
						      <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Confirm</h4>
						        </div>
						        <div class="modal-body">
						        		<input type="hidden" name="del_id" id = "del_id" value="<?php echo $row['id'] ?>">
						          <p>Are you sure you want to delete this teacher?</p>
						        </div>
						        <div class="modal-footer">
						        	<button type="submit" class="btn btn-default pull-left">Yes</button>
						         	<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						        </div>
						      </div>
					      </form>
					      
					    </div>
  					</div>
            <?php } ?>      
        </tbody>
    </table>
<!--    Pagination links-->
    <div class="text-center">

        <?php
        if (!empty($_GET)) {
            //we must unset $_GET[page] if previously built by http_build_query function
            unset($_GET['page']);
            //to keep the query sting parameters intact while navigating to next/prev page,
            $http_query = "?" . http_build_query($_GET);
        } else {
            $http_query = "?";
        }
        //Show pagination links
        if ($total_pages > 1) {
            echo '<ul class="pagination text-center">';
            for ($i = 1; $i <= $total_pages; $i++) {
                ($page == $i) ? $li_class = ' class="active"' : $li_class = "";
                echo '<li' . $li_class . '><a href="teacher_mentoring.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
            }
            echo '</ul></div>';
        }
        ?>
    </div>
    <!--    Pagination links end-->

</div>
<!--Main container end-->
<?php include_once './includes/footer.php'; ?>