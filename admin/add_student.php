<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to customers.php page.
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.

    $data_to_store = filter_input_array(INPUT_POST);
    
    //Insert timestamp
    $data_to_store['created_at'] = date('Y-m-d H:i:s');
    //$data_to_store['branch_name'] = implode( ',',$data_to_store['branch_name']);

    /* $data_to_store['password'] = md5($data_to_store['password']);
    $data_to_store['branch_name'] = implode( ',',$data_to_store['branch_name']);
    $data_to_store['course_name'] = implode( ',',$data_to_store['course_name']);
    $data_to_store['subject'] = $data_to_store['subject'];*/
    //implode( ',',$data_to_store['subject']);
    //$data_to_store['topic_name'] = implode( ',',$data_to_store['topic_name']);

    $last_id = $db->insert ('student', $data_to_store);
   
    if($last_id)
    {
    	$_SESSION['success'] = "Student added successfully!";
    	header('location: student.php');
    	exit();
    }  
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

require_once 'includes/header.php'; 
?>
<div id="page-wrapper">
<div class="row">
     <div class="col-lg-12">
            <h2 class="page-header">Add Student</h2>
        </div>
        
</div>
    <form class="form" action="add_student.php" method="post"  id="student_form" enctype="multipart/form-data">
       <?php  include_once('./includes/forms/student_form.php'); ?>
    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $("#teacher_form").validate({
       rules: {
           firstname: {
                required: true,
                minlength: 3
            },
           lastname: {
                required: true,
                minlength: 3
            },   
        }
    });
});
</script>

<script type="text/javascript">
function displayBatch(sel) {
	
	var coursename = $('#course_name').val(); 
	
	
	if (coursename.length > 0 ) { 

	 $.ajax({
			type: "POST",
			url: "fetch_batch.php",
			data: "coursename="+coursename,
			cache: false,
			beforeSend: function () { 
				//$('#output1').html('<img src="loader.gif" alt="" width="24" height="24">');
			},
			success: function(html) { 
			
				$("#branch_name").html( html );
			}
		});
	} 
}
</script>

<?php include_once 'includes/footer.php'; ?>