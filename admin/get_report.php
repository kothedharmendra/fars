<?php 
require_once './config/config.php';

function array_flatten($array) { 
  if (!is_array($array)) { 
    return false; 
  } 
  $result = array(); 
  foreach ($array as $key => $value) { 
    if (is_array($value)) { 
      $result = array_merge($result, array_flatten($value)); 
    } else { 
      $result[$key] = $value; 
    } 
  } 
  return $result; 
}
$link = mysqli_connect( $servername, $username, $password, $dbname );
if (!$link) {
    die( mysqli_error() );
}
$columnHeader = '';
 $setData = '';
$sql1="select distinct teacher_id,roll_number,branch_name,student_name,today_date from mentoring where roll_number='".$_POST["roll_number"]."' order by id desc";
$res1=mysqli_query($link,$sql1);
$rowData .= "------------------------------------------------------------------------------------------------------------------------------------------------------------------\t-----------------------------------------\t-------------------------------------------------\t\n";
while($row1=mysqli_fetch_array($res1))
{
$teacher_id=$row1["teacher_id"];

$sql_t="select concat(firstname,' ',lastname) as name,subject from teacher where id=".$teacher_id;
	$res_t=mysqli_query($link,$sql_t);
	$row_t=mysqli_fetch_array($res_t);


   
	$columnHeader = "Student Name : " . $row1["student_name"] . "\n" . "Roll Number : " . $row1["roll_number"]."\n";

 

 $rowData.= "Teacher Name : " . $row_t["name"]." (".$row_t["subject"].")" . "\n" . "Batch Name : " . $row1["branch_name"]."\n";
 $rowData .= "------------------------------------------------------------------------------------------------------------------------------------------------------------------\t-----------------------------------------\t-------------------------------------------------\t\n";

$sql="select distinct section_name,roll_number,course_name,student_name,teacher_id,today_date,start_time,end_time from mentoring where roll_number='".$_POST["roll_number"]."' and  today_date='".$row1["today_date"]."' order by id desc";
$res=mysqli_query($link,$sql);
while($row=mysqli_fetch_array($res))
{
	

	//$rowData.= "Teacher Name :".$row_t["name"]."(".$row_t["subject"].")";

	$rowData.="Section Name : " . $row["section_name"] . "\n" ;
	//$rowData .= "Date : " . $row["today_date"] . "\n" . "Start Time : " . $row["start_time"]."\n" . "End Time : " . $row["end_time"]."\n";
	//$rowData .= "----------------------\t-------------------\t-------------------------\t\n";
	$sql_ques="select question_data,concat(left(subject, 1),'',teacher_id,'-',date_format(today_date,'%d %b %y'),'-',start_time,'-',end_time) as teacher_code  from mentoring,teacher where teacher_id=teacher.id and section_name='".$row["section_name"]."' and roll_number = '".$_POST["roll_number"]."' and teacher_id='".$row["teacher_id"]."' and today_date='".$row1["today_date"]."' order by mentoring.id desc";
	
	$res_ques=mysqli_query($link,$sql_ques);
	$row_ques=mysqli_fetch_array($res_ques);
	$ques_data=$row_ques["question_data"];
	$arr=json_decode($ques_data);
	//print_r($arr);
	if($ques_data!="")
	{
	$rowData.= "Teacher Code \t Question " . "\t" . "Answer " . "\t" . "Comment"."\n";
	for($i=0;$i<count($arr);$i++)
	{

		//if($arr[$i]->question_name!="")
		{
			//$value = '"' . $value . '"' . "\t";
            $rowData .= $row_ques["teacher_code"]."\t".$arr[$i]->question_name."\t".$arr[$i]->answer."\t".$arr[$i]->comment."\n";
		
		}
	 }
	}
	$rowData .= "\n";  
}
 $rowData .= "------------------------------------------------------------------------------------------------------------------------------------------------------------------\t-----------------------------------------\t-------------------------------------------------\t\n";

}

 $setData .= trim( $rowData ) . "\n";
    header( "Content-Type: application/xls" );
    header( 'Content-Disposition: attachment; filename=Academic_review.xls' );
    header( "Pragma: no-cache" );
    header( "Expires: 0" );
    echo ucwords( $columnHeader ) . "\n" . $setData . "\n";

