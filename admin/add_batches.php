<?php error_reporting(E_ALL);
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to customers.php page.
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $data_to_store = filter_input_array(INPUT_POST);
    //Insert timestamp
    $data_to_store['created_at'] = date('Y-m-d H:i:s');
   ;
    $data_to_store['course_master_id'] = $_POST['course_master_id'];
    $data_to_store['branch_name'] = $_POST['branch_name'];
   // $data_to_store['course_name'] = $_POST['course_name'];
	$data_to_store['status'] = $_POST['status'];
    
    //$data_to_store['topic_name'] = implode( ',',$data_to_store['topic_name']);

    $last_id = $db->insert ('batch', $data_to_store);
    if($last_id)
    {
    	$_SESSION['success'] = "Batch added successfully!";
    	header('location: batches.php');
    	exit();
    }  
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

require_once 'includes/header.php'; 
?>
<div id="page-wrapper">
<div class="row">
     <div class="col-lg-12">
            <h2 class="page-header">Add Batch</h2>
        </div>
        
</div>
    <form class="form" action="" method="post"  id="teacher_form" enctype="multipart/form-data">
       <fieldset>
	   <div class="form-group">
        <label for="topic_name">Batch Name *</label>
        <input type="text" name="branch_name" value="<?php echo $edit ? $customer['branch_name'] : ''; ?>"
               placeholder="Enter Batch Name" class="form-control" required="required" id="branch_name">
    </div>
    <?php
    $cols = array("id","course_name");
    //$course = $db->get( "course", null, $cols );
    $query = "SELECT id,course_name FROM course_master WHERE status=1";
    $course = $db->rawQuery( $query );
    $usersCourse = explode( ",", $customer['course_name'] );
    $db->where( "status", 1 );
    if ($db->count > 0) : ?>
        <div class="form-group">
            <label>Course *</label>
            <select name="course_master_id" class="form-control selectpicker" required="required" >
				<option value="">Select Course</option>
                <?php
                foreach ($course as $value) {
                    if ($edit && in_array( $value['course_name'], $usersCourse )) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo '<option value="' . $value['id'] . '"' . $sel . '>' . $value['course_name'] . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif; ?>
    
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $customer['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>

    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $("#teacher_form").validate({
       rules: {
           course_name: {
                required: true
               
            },
			branch_name: {
                required: true,
                minlength: 3
            },  
        }
    });
});
</script>

<?php include_once 'includes/footer.php'; ?>