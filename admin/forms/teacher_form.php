<fieldset>
    <div class="form-group">
        <label>Title </label>
        <?php $opt_arr = array("Mr.", "Mrs.", "Ms.", "Miss.","Dr."); ?>
        <select name="title" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt) {
                if ($edit && $opt == $customer['title']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $opt . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="firstname">First Name *</label>
        <input type="text" name="firstname" value="<?php echo $edit ? $customer['firstname'] : ''; ?>"
               placeholder="First Name" class="form-control" required="required" id="firstname">
    </div>
    <div class="form-group">
        <label for="lastname">Last name *</label>
        <input type="text" name="lastname" value="<?php echo $edit ? $customer['lastname'] : ''; ?>"
               placeholder="Last Name" class="form-control" required="required" id="lastname">
    </div>
    <div class="form-group">
        <label>Location </label>
        <?php $opt_arr = array("Nagpur", "Pune"); ?>
        <select name="location" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt) {
                if ($edit && $opt == $customer['location']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $opt . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="email">Email *</label>
        <input type="email" name="email" value="<?php echo $edit ? $customer['email'] : ''; ?>"
               placeholder="E-Mail Address" class="form-control" id="email" required="required">
    </div>
    <div class="form-group">
        <label for="phone">Phone *</label>
        <input name="phone" value="<?php echo $edit ? $customer['phone'] : ''; ?>" placeholder="9890267215"
               class="form-control" type="text" id="phone" required="required"/>
    </div>
    <!-- Text input-->
    <div class="form-group">
        <label for="password">Password *</label>
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="password" name="password" placeholder="Password " class="form-control" autocomplete="off">
        </div>
    </div>
    <?php
    $cols = array("course_year");
    //$course = $db->get( "course", null, $cols );
    $query = "SELECT id,course_name FROM course_master WHERE status=1";
    $course = $db->rawQuery( $query );
    $usersCourse = explode( ",", $customer['course_name'] );
    $db->where( "status", 1 );
    if ($db->count > 0) : ?>
        <div class="form-group">
            <label>Course</label>
            <select name="course_name[]" id="course_name" class="form-control selectpicker" required="required" multiple onchange="javascript:displayBatch(this)">
                <?php
                foreach ($course as $value) {
                    if ($edit && in_array( $value['course_name'], $usersCourse )) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo '<option value="' . $value['course_name'] . '"' . $sel . '>' . $value['course_name'] . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <label>Subject</label>
        <?php $opt_arr = array("Biology", "Physics", "Chemistry", "Math"); ?>
        <select name="subject" class="form-control selectpicker" required >
            <option value="">Select Subject</option>
            <?php
            $usersSubject = explode( ",", $customer['subject'] );
            foreach ($opt_arr as $value) {
                if ($edit && in_array($value, $usersSubject)) {
                        $sel = "selected";
                } else {
                    $sel = "";
                }

                echo '<option value="' . $value . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>

    <?php
     if ($edit)
     {
		$usersBranch = explode(",", $customer['branch_name']);
		$courseids="";

    foreach($usersCourse as $usersCourse)
    {
        $querycourse = "SELECT id FROM course_master WHERE course_name='".$usersCourse."'";
        $courseid = $db->rawQuery( $querycourse );

        $courseids.=$courseid[0]['id'].",";
    }


       $selectedbatchquery = "SELECT * FROM batch WHERE course_master_id IN(".rtrim($courseids,",").")";
       $selectedbatch = $db->rawQuery($selectedbatchquery );
}

    if ($db->count > 0) : ?>
        <div class="form-group">
            <label>Batches</label>
            <select name="branch_name[]" class="form-control selectpicker" required="required" multiple id="branch_name">
                <?php
               if ($edit)
               {
                foreach ($selectedbatch as $value) {
                    if ($edit && in_array( $value['branch_name'], $usersBranch )) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo '<option value="' . $value['branch_name'] . '"' . $sel . '>' . $value['branch_name'] . '</option>';
                }
                }
                ?>
            </select>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <label>Allow Pastdate</label>
        <?php $opt_arr = array("1" => "Yes", "0" => "No"); ?>
        <select name="allow_pastdate" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $customer['allow_pastdate']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $customer['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>


