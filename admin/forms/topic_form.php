<fieldset>
    <div class="form-group">
        <label for="topic_name">Topic Name *</label>
        <input type="text" name="topic_name" value="<?php echo $edit ? $customer['topic_name'] : ''; ?>"
               placeholder="Enter Topic Name" class="form-control" required="required" id="topic_name">
    </div>
    <div class="form-group">
        <label for="number_lectures">NO. OF LECTURES *</label>
        <input type="text" name="number_lectures" value="<?php echo $edit ? $customer['number_lectures'] : ''; ?>"
               placeholder="Enter NO. OF LECTURES" class="form-control" required="required" id="number_lectures">
    </div>
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $customer['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>
