<fieldset>
    <div class="form-group">
        <!-- <label for="course_year">Course Year *</label> -->
        <!-- <input type="text" name="course_year" value="<?php echo $edit ? $customer['course_year'] : ''; ?>"
               placeholder="Course Year" class="form-control" required="required" id="course_year"> -->
<?php
	//$course = $db->get( "course", null, $cols );
    $query = "SELECT id,course_name FROM course_master WHERE status=1";
    $course = $db->rawQuery( $query );
    $db->where( "status", 1 );
    if ($db->count > 0) : ?>
        <div class="form-group">
            <label>Course *</label>
			<?php //echo $customer['course_year']; ?>
            <select name="course_year" class="form-control selectpicker" required="required" >
			<option value="">Select Course</option>
                <?php
                foreach ($course as $value) {
                    if ($edit && $customer['course_year'] == $value['course_name']) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo '<option value="' . $value['course_name'] . '"' . $sel . '>' . $value['course_name'] . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif; ?>
    </div>
    <div class="form-group">
        <label>Subject </label>
        <?php $opt_arr = array("Physics", "Math","Biology","Chemistry");?>
        <select name="subject" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt) {
                if ($edit && $opt == $customer['subject']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $opt . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="seq_no">Sequence Number *</label>
        <input type="text" name="seq_no" value="<?php echo $edit ? $customer['seq_no'] : ''; ?>"
               placeholder="Enter sequence number" class="form-control" required="required" id="seq_no">
    </div>
    <div class="form-group">
        <label for="topic_name">Name Of Topic *</label>
        <input type="text" name="topic_name" value="<?php echo $edit ? $customer['topic_name'] : ''; ?>"
               placeholder="Enter Name Of Topic" class="form-control" required="required" id="topic_name">
    </div>
    <div class="form-group">
        <label for="no_lecture">No Of Lecture *</label>
        <input type="text" name="no_lecture" value="<?php echo $edit ? $customer['no_lecture'] : ''; ?>"
               placeholder="Enter No Of Lecture" class="form-control" required="required" id="no_lecture">
    </div>

    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
                foreach ($opt_arr as $opt => $value) {
                    if ($edit && $opt == $customer['status']) {
                        $sel = "selected";
                    } else {
                        $sel = "";
                    }
                    echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
                }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>
