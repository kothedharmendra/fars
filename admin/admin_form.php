<fieldset>
    <div class="form-group">
        <label for="topic_name">Name *</label>
        <input type="text" name="name" value="<?php echo $edit ? $customer['name'] : ''; ?>"
               placeholder="Enter Name" class="form-control" required="required" id="topic_name">
    </div>
    <div class="form-group">
        <label for="number_lectures">Email *</label>
        <input type="text" name="email" value="<?php echo $edit ? $customer['email'] : ''; ?>"
               placeholder="Enter Email" class="form-control" required="required" id="number_lectures">
    </div>
	<div class="form-group">
        <label for="number_lectures">Password *</label>
        <input type="text" name="password" value="<?php echo $edit ? $customer['password'] : ''; ?>"
                class="form-control" required="required" id="number_lectures">
    </div>
    <div class="form-group">
        <label>Modules</label>
       <ul>
	     <li><INPUT TYPE="checkbox" NAME="chkdash" value="1">&nbsp;Dashboard</li>
		 <li><INPUT TYPE="checkbox" NAME="chkdash" value="1">&nbsp;Teacher</li>
		 <li><INPUT TYPE="checkbox" NAME="chkdash" value="1">&nbsp;Masters</li>
		 <li><INPUT TYPE="checkbox" NAME="chkdash" value="1">&nbsp;Reports</li>
		 <li><INPUT TYPE="checkbox" NAME="chkdash" value="1">&nbsp;Manage Admin</li>
        </ul>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>
