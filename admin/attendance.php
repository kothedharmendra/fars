<?php

// Report all PHP errors
error_reporting(-1);

session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';
include_once 'includes/header.php'; 
$link = mysqli_connect($servername, $username, $password, $dbname);
if(!$link) {
	die(mysqli_error());
}

//$resBatch = getAllBatchAction($link);
if($row['member_type']=="U")
{
  $arr=explode(",", $row["batches"]);
$resBatch = getAllBatchAction( $link ,$row["member_type"],$row["batches"]);
$resTeacher = getAllTeacherAction($link,$row["member_type"],$row["batches"]);
}
else
{
$resBatch = getAllBatchAction( $link ,$row["member_type"]);
$resTeacher = getAllTeacherAction($link);
}
/* echo "<pre>";
print_r($res);
echo "</pre>";
*/


//Get Input data from query string
$search_string = filter_input( INPUT_GET, 'search_string' );
$filter_col = filter_input( INPUT_GET, 'filter_col' );
$order_by = filter_input( INPUT_GET, 'order_by' );
//Get current page.
$page = filter_input( INPUT_GET, 'page' );
//Per page limit for pagination.
$pagelimit = 150;
if (!$page) {
    $page = 1;
}
// If filter types are not selected we show latest added data first
if (!$filter_col) {
    $filter_col = "id";
}
if (!$order_by) {
    $order_by = "DESC";
}

// select the columns
$select = array('id', 'teacher_id', 'batch', 'date', 'subject',
    'topic_taught', 'present_head_count', 'doubt_clearing_count', 'mentoring_count', 'start_time', 'end_time',
    'lecture_no', 'total_lecture_no','sequence_no','balence_lecture','created_at');

/* $attendance = $db->rawQuery("SELECT u.title,u.firstname,u.lastname,
     p.id, p.branch,p.batch,p.date,p.subject,c.topic_name,
    p.topic_taught,p.present_head_count,p.doubt_clearing_count,p.mentoring_count,
    p.start_time ,p.end_time,p.lecture_no,p.created_at FROM `attendance` AS p
    INNER JOIN teacher AS u ON u.id= p.teacher_id
	INNER JOIN course as c on p.topic_taught=c.id ORDER BY p.id DESC LIMIT $page, $pagelimit");*/


//Start building query according to input parameters.
// If search string
if ($search_string) {
    $db->where( 'branch', '%' . $search_string . '%', 'like' );
    $db->orwhere( 'subject', '%' . $search_string . '%', 'like' );
    $db->orwhere( 'date', '%' . $search_string . '%', 'like' );
    $db->orwhere( 'batch', '%' . $search_string . '%', 'like' );
    $db->orwhere( 'topic_taught', '%' . $search_string . '%', 'like' );
}
//$db->where('date', );
$db->where('date', '%DATE_SUB(CURDATE(), INTERVAL 1 MONTH)%', '>=' );

 //$db->where( "1=1" );

 if($row['member_type']=="U")
{
	$cond="";
  $arr=explode(",", $row['batches']);
  //print_r($arr);
/*$str=array();
$str="";
for($i=0;$i<count($arr);$i++)
{
	if($str=="")
	   $str="('".$arr[$i]."'";
	else
		$str.=",'".$arr[$i]."'";
}
$str.=")";*/
//$str="'EXCEL-PMT19-BB1','Excel-PMT20-BB2'";
//$db->where_in('batch',$str);
}

//If order by option selected
if ($order_by) {
    $db->orderBy( $filter_col, $order_by );
}

//Set pagination limit
$db->pageLimit = $pagelimit;

//Get result of the query.
$teachers = $db->arraybuilder()->paginate( "attendance", $page, $select );

//print_r($teacher);
$total_pages = $db->totalPages;

// get columns for order filter
foreach ($teachers as $value) {
    foreach ($value as $col_name => $col_value) {
        $filter_options[$col_name] = $col_name;
    }
    //execute only once
    break;
}
?>
    <!--Main container start-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">All Lecture Details</h1>
            </div>
        </div>
        <?php include('./includes/flash_messages.php') ?>
        <!--    Begin filter section-->
        <div class="well text-center filter-form">
            <form class="form form-inline" action="">
                <!--<label for="input_search">Search</label>
                <input type="text" class="form-control" id="input_search" name="search_string"
                       value="<?php echo $search_string; ?>">
                <label for="input_order">Order By</label>
                <select name="filter_col" class="form-control">
                    <?php
                    foreach ($filter_options as $option) {
                        ($filter_col === $option) ? $selected = "selected" : $selected = "";
                        echo ' <option value="' . $option . '" ' . $selected . '>' . $option . '</option>';
                    }
                    ?>
                </select>
                <select name="order_by" class="form-control" id="input_order">
                    <option value="Asc" <?php
                    if ($order_by == 'Asc') {
                        echo "selected";
                    }
                    ?> >Asc
                    </option>
                    <option value="Desc" <?php
                    if ($order_by == 'Desc') {
                        echo "selected";
                    }
                    ?>>Desc
                    </option>
                </select>
                <input type="submit" value="Go" class="btn btn-primary">-->
                <button type="button" class="btn btn-info btn-lg"
                        data-toggle="modal" data-target="#myModal">Export Complete Report
                </button>
				
				 <button type="button" class="btn btn-info btn-lg"
                        data-toggle="modal" data-target="#myModalTeacher"> Export Teacher Report
                </button>
                <button type="button" class="btn btn-info btn-lg"
                        data-toggle="modal" data-target="#myModalBatch"> Export Batch Report
                </button>

                <!-- <a href="export.php?time=<?php echo time(); ?>" >Export Record</a>-->
            </form>
        </div>
        <!--   Filter section end-->
        <hr/>
        <table class="table table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th class="header">#</th>
                <th>Teacher Name</th>
                <!--<th>Branch</th>-->
                <th>Batch</th>
                <th>Date</th>
                <th>Subject</th>
                <th>Sequence No</th>
                <th>Topic Ttaught</th>
                <th>Head Count</th>
                <th>Clearing Count</th>
                <th>Mentoring Count</th>
                <th>Start Time</th>
                <th>End Time</th>
                <th>​Total Numbers of Lectures</th>
                <th>Total Lecture</th>
                <th>​Balance Lecture </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($teachers as $row) { ?>
                <tr>
                    <td><?php echo $row['id'] ?></td>
                    <td><?php echo getTeacher( $row['teacher_id'], $db ); ?></td>
                    <td><?php echo $row['batch'] ?></td>
                    <td><?php echo date('d-m-Y',strtotime($row['date'])); ?></td>
                    <td><?php echo $row['subject'] ?></td>
                    <td><?php echo $row['sequence_no'] ?></td>
                    <td><?php echo getTopic( $row['topic_taught'], $db ); ?></td>
                    <td><?php echo $row['present_head_count'] ?></td>
                    <td><?php echo $row['doubt_clearing_count'] ?></td>
                    <td><?php echo $row['mentoring_count'] ?></td>
                    <td><?php echo $row['start_time'] ?></td>
                    <td><?php echo $row['end_time'] ?></td>
                    <td><?php echo $row['total_lecture_no'] ?></td>
					<td><?php echo $row['lecture_no'] ?></td>
                    <td><?php echo $row['balence_lecture']; //getBalance($row['total_lecture_no'],$row['lecture_no']); ?></td>
                </tr>
                <!-- Delete Confirmation Modal-->
                <div class="modal fade" id="confirm-delete-<?php echo $row['id'] ?>" role="dialog">
                    <div class="modal-dialog">
                        <form action="delete_teacher.php" method="POST">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Confirm</h4>
                                </div>
                                <div class="modal-body">
                                    <input type="hidden" name="del_id" id="del_id" value="<?php echo $row['id'] ?>">
                                    <p>Are you sure you want to delete this teacher?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-default pull-left">Yes</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            <?php } ?>
            </tbody>
        </table>
        <!--    Pagination links-->
        <div class="text-center">
            <?php
            if (!empty( $_GET )) {
                //we must unset $_GET[page] if previously built by http_build_query function
                unset( $_GET['page'] );
                //to keep the query sting parameters intact while navigating to next/prev page,
                $http_query = "?" . http_build_query( $_GET );
            } else {
                $http_query = "?";
            }
            //Show pagination links
            if ($total_pages > 1) {
                echo '<ul class="pagination text-center">';
                for ($i = 1; $i <= $total_pages; $i++) {
                    ($page == $i) ? $li_class = ' class="active"' : $li_class = "";
                    echo '<li' . $li_class . '><a href="attendance.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
                }
                echo '</ul></div>';
            }
            ?>
        </div>
        <!--    Pagination links end-->

    </div>
    <!--Main container end-->
    <!-- Trigger the modal with a button -->

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Export Report</h4>
            </div>
            <form name="frm" id="frm" method="post" action="export.php?time=<?php echo time(); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">From Date:</label>
                        <input type="date" class="form-control" id="from_date"
                               min="<?php echo $min = date('d-m-Y');?>" name="from_date" class="datepicker"required/>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">To Date:</label>
                        <input type="date" class="form-control" id="to_date"
                               max="<?php echo $min = date('d-m-Y');?>" name="to_date" class="datepicker" required/>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input value="Export" class="btn btn-primary" type="submit" />
                    </div>
                </div>
            </form>
        </div>

    </div>
	</div>
	<div id="myModalTeacher" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Export Report</h4>
            </div>
            <form name="frm" id="frm" method="post" action="export.php?time=<?php echo time(); ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="usr">Select Teacher</label>
						            <select name="teacher_export" class="form-control selectpicker" required="required" >
						<?php 
						foreach ($resTeacher as $value) {
						echo '<option value="' . $value[0] . '">' . $value[1] . '</option>';
					}
				?>
				</select>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input value="Export" class="btn btn-primary" type="submit" />
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
    <div id="myModalBatch" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export Report</h4>
                </div>
                <form name="frm" id="frm" method="post" action="export.php?time=<?php echo time(); ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Select Batch</label>
                            <select name="branch_name" class="form-control selectpicker" required="required" >
                                <?php
                                foreach ($resBatch as $value) {
                                    echo '<option value="' . $value[1] . '">' . $value[1] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input value="Export" class="btn btn-primary" type="submit" />
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>


<?php

function getTeacher($cn, $db)
{
    $query = "SELECT CONCAT(`title` ,' ',`firstname` ,' ',`lastname`) as name FROM `teacher` WHERE id='" . $cn . "'";
    $teacherName = $db->rawQuery( $query );
    return $teacherName[0]['name'];
}

/**
 * @param $cn
 * @param $db
 * @return mixed
 */
function getTopic($cn, $db)
{
    $query = "SELECT topic_name as name FROM `course` WHERE id='" . $cn . "'";
    $teacherName = $db->rawQuery($query);
    return $teacherName[0]['name'];
}

function getBalance($total, $present)
{
	$result = '';
	if($total >= $present) {
		$result = $total - $present;
	} else if($total < $present) {
		$result =  '+ '.($present - $total);
	}
	return $result;
}

//View Report
function getAllTeacherAction($link,$member_type='',$batches='')
 {
	 if($member_type=="U")
{
	$cond="";
  $arr=explode(",", $batches);
$str="";
for($i=0;$i<count($arr);$i++)
{
	if($str=="")
	   $str=" and ( branch_name like '%".$arr[$i]."%'";
	else
		$str.=" or branch_name like '%".$arr[$i]."%'";
}
$str.=")";
}
	 $data = array();
	$sql = "SELECT id, CONCAT(title,' ',firstname,' ',lastname) AS fname FROM `teacher` Where status=1 AND is_delete=0".$str;
	$result = mysqli_query($link, $sql);
		while ($rec = mysqli_fetch_row($result)) {
			  $data[] = $rec;
		   }
	return $data;
}

//View Report
function getAllBatchAction($link,$member_type,$batches='')
{
if($member_type=="U")
{
	$cond="";
  $arr=explode(",", $batches);
$str="";
for($i=0;$i<count($arr);$i++)
{
	if($str=="")
	   $str="'".$arr[$i]."'";
	else
		$str.=",'".$arr[$i]."'";
}
$cond=" and branch_name in (".$str.")"; 
}
	 $data = array();
	$sql = "SELECT id, branch_name FROM `batch` Where status=1".$cond;
	$result = mysqli_query($link, $sql);
		while ($rec = mysqli_fetch_row($result)) {
			  $data[] = $rec;
		   }
	return $data;
}

?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format: 'mm-dd-yyyy',
            endDate: '+0d',
            autoclose: true
        });
    });
    $(document).ready(function(){
        $("#frm").validate({
            $('.datepicker').datepicker({
            format: 'mm-dd-yyyy',
            endDate: '+0d',
            autoclose: true
        });
    });
    });
</script>
<?php include_once './includes/footer.php'; ?>