<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to customers.php page.
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $data_to_store = filter_input_array(INPUT_POST);
    //Insert timestamp
	echo $_POST['name'];
	
    $data_to_store['name'] = $_POST['name'];
	$data_to_store['email'] = $_POST['email'];
	$data_to_store['password'] = md5($_POST['password']);
	$data_to_store['status'] = '1';
    $data_to_store['dashboard'] = $_POST['chkdash'];
	$data_to_store['teacher'] = $_POST['chkteach'];
	$data_to_store['masters'] = $_POST['chkmaster'];
	$data_to_store['reports'] = $_POST['chkreport'];
	$data_to_store['admin_user'] = $_POST['chkadmin'];
	$data_to_store['is_delete'] = '0';
	
    $last_id = $db->insert ('admin_accounts', $data_to_store);
    if($last_id)
    {
    	$_SESSION['success'] = "Admin added successfully!";
    	header('location: manage_user.php');
    	exit();
    }  
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

require_once 'includes/header.php'; 
?>
<div id="page-wrapper">
<div class="row">
     <div class="col-lg-12">
            <h2 class="page-header">Add Admin</h2>
        </div>
        
</div>
    <form class="form" action="" method="post"  id="topic_form" enctype="multipart/form-data">
       <?php  include_once('./includes/forms/admin_form.php'); ?>
    </form>
</div>


<script type="text/javascript">
$(document).ready(function(){
   $("#topic_form").validate({
       rules: {
           firstname: {
                required: true,
                minlength: 3
            },
           lastname: {
                required: true,
                minlength: 3
            },   
        }
    });
});
</script>

<?php include_once 'includes/footer.php'; ?>