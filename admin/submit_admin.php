<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to customers.php page.
if ($_REQUEST['admin_id']!="") 
{
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.

 $customer_id = filter_input(INPUT_GET, 'admin_id', FILTER_SANITIZE_STRING);
// echo $_POST['chkdash'.$customer_id];


    $data_to_store = filter_input_array(INPUT_POST);
    
    //Insert timestamp
   $data_to_update['dashboard'] = $_POST['chkdash'.$customer_id];
   $data_to_update['teacher'] = $_POST['chkteach'.$customer_id];
   $data_to_update['masters'] = $_POST['chkmaster'.$customer_id];
   $data_to_update['reports'] = $_POST['chkreport'.$customer_id];
   $data_to_update['admin_user'] = $_POST['chkadmin'.$customer_id];
    $db->where('id',$customer_id);
    $stat = $db->update('admin_accounts', $data_to_update);
   
    if($stat)
    {
    	$_SESSION['success'] = "Admin details edited successfully!";
    	header('location: manage_user.php');
    	exit();
    }  
	header('location: manage_user.php');
    	exit();
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

?>