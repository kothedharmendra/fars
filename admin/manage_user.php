<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

//Get Input data from query string
$search_string = filter_input(INPUT_GET, 'search_string');
$filter_col = filter_input(INPUT_GET, 'filter_col');
$order_by = filter_input(INPUT_GET, 'order_by');
//Get current page.
$page = filter_input(INPUT_GET, 'page');
//Per page limit for pagination.
$pagelimit = 20;
if (!$page) {
    $page = 1;
}
// If filter types are not selected we show latest added data first
if (!$filter_col) {
    $filter_col = "created_at";
}
if (!$order_by) {
    $order_by = "Desc";
}

// select the columns
$select = array('id', 'name','email','dashboard','teacher',
    'masters','reports','admin_user','status'
);
//$db->where('is_delete','0');


//Start building query according to input parameters.
// If search string
if ($search_string) 
{
    $db->where('name', '%' . $search_string . '%', 'like');    
    $db->orwhere('status', '%' . $search_string . '%', 'like');
}
    $db->where('is_delete','0');
    $db->where('member_type','A');

//Set pagination limit
$db->pageLimit = $pagelimit;

//Get result of the query.
$teacher = $db->arraybuilder()->paginate("admin_accounts", $page, $select);
//print_r($teacher);
$total_pages = $db->totalPages;

include_once 'includes/header.php'; ?>
<!--Main container start-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h1 class="page-header">Admin Users</h1>
        </div>
        <div class="col-lg-6" style="">
            <div class="page-action-links text-right">
	            <a href="add_teacher.php?operation=create">
	            	<button class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add new </button>
	            </a>
            </div>
        </div>
    </div>
        <?php include('./includes/flash_messages.php') ?>
    <!--    Begin filter section-->
    <div class="well text-center filter-form">
        <form class="form form-inline" action="">
            <label for="input_search">Search</label>
            <input type="text" class="form-control" id="input_search" name="search_string" value="<?php echo $search_string; ?>">
            <input type="submit" value="Go" class="btn btn-primary">
        </form>
    </div>
<!--   Filter section end-->
    <hr />
    <FORM METHOD=POST name="frmadmin">
		<table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
             <!--    <th class="header">#</th> -->
				<th>Code</th>               
                <th>Name</th>
                <th>Email</th>
                <th>Dashboard Menu</th>
                <th>Teacher Menu</th>
                
                <th>Masters</th>				
                <th>Reports</th>
				<th>Manage Admin</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($teacher as $row) { ?>
                <tr>              
					<td><?php echo $row['id']; ?> </td>
	                <td><?php echo $row['name']; ?></td>
	                <td><?php echo $row['email'] ?></td>
	                <td><INPUT TYPE="checkbox" NAME="chkdash<?php echo $row['id']; ?>" value="1" <?php if($row['dashboard']=="1") echo "checked";?>></td>
	                <td><INPUT TYPE="checkbox" NAME="chkteach<?php echo $row['id']; ?>" value="1" <?php if($row['teacher']=="1") echo "checked";?>> </td>
	                <td><INPUT TYPE="checkbox" NAME="chkmaster<?php echo $row['id']; ?>" value="1" <?php if($row['masters']=="1") echo "checked";?>> </td>
	                
	                <td><INPUT TYPE="checkbox" NAME="chkreport<?php echo $row['id']; ?>" value="1" <?php if($row['reports']=="1") echo "checked";?>> </td>
					<td><INPUT TYPE="checkbox" NAME="chkadmin<?php echo $row['id']; ?>" value="1" <?php if($row['admin_user']=="1") echo "checked";?>> </td>
	               
	                <td>
					<a href="#" class="btn btn-primary" style="margin-right: 8px;" onclick="editadmin(<?php echo $row['id']; ?>)" id="editadmin">
                        <span class="glyphicon glyphicon-edit"></span>
					<a href=""  class="btn btn-danger delete_btn" data-toggle="modal" data-target="#confirm-delete-<?php echo $row['id'] ?>" style="margin-right: 8px;"><span class="glyphicon glyphicon-trash"></span></td>
				</tr>
            		<!-- Delete Confirmation Modal-->
					 <div class="modal fade" id="confirm-delete-<?php echo $row['id'] ?>" role="dialog">
					    <div class="modal-dialog">
					      <form action="delete_admin.php" method="POST">
					      <!-- Modal content-->
						      <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Confirm</h4>
						        </div>
						        <div class="modal-body">
						        		<input type="hidden" name="del_id" id = "del_id" value="<?php echo $row['id'] ?>">
						          <p>Are you sure you want to delete this teacher?</p>
						        </div>
						        <div class="modal-footer">
						        	<button type="submit" class="btn btn-default pull-left">Yes</button>
						         	<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						        </div>
						      </div>
					      </form>
					      
					    </div>
  					</div>
            <?php } ?>      
        </tbody>
    </table>
    </FORM>
<!--    Pagination links-->
    <div class="text-center">

        <?php
        if (!empty($_GET)) {
            //we must unset $_GET[page] if previously built by http_build_query function
            unset($_GET['page']);
            //to keep the query sting parameters intact while navigating to next/prev page,
            $http_query = "?" . http_build_query($_GET);
        } else {
            $http_query = "?";
        }
        //Show pagination links
        if ($total_pages > 1) {
            echo '<ul class="pagination text-center">';
            for ($i = 1; $i <= $total_pages; $i++) {
                ($page == $i) ? $li_class = ' class="active"' : $li_class = "";
                echo '<li' . $li_class . '><a href="teacher.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
            }
            echo '</ul></div>';
        }
        ?>
		<script>
		function editadmin(id){
			if(confirm('Are you sure you want to update the record?')) 	
			{
			   document.frmadmin.action='submit_admin.php?admin_id='+id+'&operation=edit';
			   document.frmadmin.submit();
			   return true; 
			}
			else 
				return false;
		}
		</script>
		
    </div>
    <!--    Pagination links end-->

</div>
<!--Main container end-->
<?php include_once './includes/footer.php'; ?>