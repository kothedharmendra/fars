<?php
session_start();
require_once './config/config.php';
require_once './includes/auth_validate.php';

//serve POST method, After successful insert, redirect to customers.php page.
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    //Mass Insert Data. Keep "name" attribute in html form same as column name in mysql table.
    $data_to_store = filter_input_array(INPUT_POST);
    //Insert timestamp
    $data_to_store['created_at'] = date('Y-m-d H:i:s');
   ;
    
    $data_to_store['course_name'] = $_POST['course_name'];
	$data_to_store['status'] = $_POST['status'];
    
    //$data_to_store['topic_name'] = implode( ',',$data_to_store['topic_name']);

    $last_id = $db->insert ('course_master', $data_to_store);
    if($last_id)
    {
    	$_SESSION['success'] = "Course added successfully!";
    	header('location: courses.php');
    	exit();
    }  
}

//We are using same form for adding and editing. This is a create form so declare $edit = false.
$edit = false;

require_once 'includes/header.php'; 
?>
<div id="page-wrapper">
<div class="row">
     <div class="col-lg-12">
            <h2 class="page-header">Add Course</h2>
        </div>
        
</div>
    <form class="form" action="" method="post"  id="teacher_form" enctype="multipart/form-data">
       <fieldset>
    <div class="form-group">
        <label for="topic_name">Course Name *</label>
        <input type="text" name="course_name" value="<?php echo $edit ? $customer['course_name'] : ''; ?>"
               placeholder="Enter Course Name" class="form-control" required="required" id="course_name">
    </div>
    
    <div class="form-group">
        <label>Status</label>
        <?php $opt_arr = array("1" => "Enable", "0" => "Disable"); ?>
        <select name="status" class="form-control selectpicker" required>
            <?php
            foreach ($opt_arr as $opt => $value) {
                if ($edit && $opt == $customer['status']) {
                    $sel = "selected";
                } else {
                    $sel = "";
                }
                echo '<option value="' . $opt . '"' . $sel . '>' . $value . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group text-center">
        <label></label>
        <button type="submit" class="btn btn-warning">Save <span class="glyphicon glyphicon-send"></span></button>
    </div>
</fieldset>

    </form>
</div>
<script type="text/javascript">
$(document).ready(function(){
   $("#teacher_form").validate({
       rules: {
           course_name: {
                required: true,
                minlength: 3
            },   
        }
    });
});
</script>

<?php include_once 'includes/footer.php'; ?>