<?php 
session_start();
require_once 'includes/auth_validate.php';
require_once './config/config.php';
$del_id = filter_input(INPUT_POST, 'del_id');
if ($del_id && $_SERVER['REQUEST_METHOD'] == 'POST') 
{
    $customer_id = $del_id;
    $db->where('id', $customer_id);
	$data_to_update['is_delete'] = 1;
	foreach($data_to_update as $key=>$value)
    {
        if(is_null($value) || $value == '')
            unset($data_to_update[$key]);
    }
    
    $status = $db->update('admin_accounts', $data_to_update);
	
    if ($status)
    {
        $_SESSION['info'] = "Admin deleted successfully!";
        header('location: manage_user.php');
        exit;
    }
    else
    {
    	$_SESSION['failure'] = "Unable to delete Admin";
    	header('location: manage_user.php');
        exit;

    }
}
