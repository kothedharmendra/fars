<?php

// Report all PHP errors
error_reporting( -1 );

session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

$link = mysqli_connect( $servername, $username, $password, $dbname );
if (!$link) {
    die( mysqli_error() );
}


include_once 'includes/header.php';

$resTeacher = getAllTeacherAction( $link );
if($row['member_type']=="U")
{
  $arr=explode(",", $row["batches"]);
$resBatch = getAllBatchAction( $link ,$row["member_type"],$row["batches"]);
}
else
{
$resBatch = getAllBatchAction( $link ,$row["member_type"]);
}
//print_r($row["batches"]);


?>
    <!--Main container start-->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6">
                <h1 class="page-header">Attendance Details</h1>
            </div>
        </div>
        <?php include('./includes/flash_messages.php') ?>
        <!--    Begin filter section-->
        <div class="well text-center filter-form">
            <form class="form form-inline" action="">
                <form class="form form-inline" action="">
                   <!--  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Export
                        All Result
                    </button> -->
                    <button type="button" class="btn btn-info btn-lg"
                            data-toggle="modal" data-target="#myModalTeacher"> Export Student Data
                    </button> <!--data-toggle="modal" data-target="#myModalBatch"-->
                    <button type="button" class="btn btn-info btn-lg"
                            data-toggle="modal" data-target="#myModalBatch"> Export Batch Data
                    </button>
                </form>
            </form>
        </div>
        <!--   Filter section end-->
        <hr/>
    </div>
    <!--Main container end-->
    <!-- Trigger the modal with a button -->

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export Report</h4>
                </div>
                <form name="frm" id="frm" method="post" action="exportAttendance.php?time=<?php echo time(); ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">From Date:</label>
                            <input type="date" class="form-control" id="from_date"
                                   min="<?php echo $min = date( 'd-m-Y' ); ?>" name="from_date" class="datepicker"
                                   required/>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">To Date:</label>
                            <input type="date" class="form-control" id="to_date"
                                   max="<?php echo $min = date( 'd-m-Y' ); ?>" name="to_date" class="datepicker"
                                   required/>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input value="Export" class="btn btn-primary" type="submit"/>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div id="myModalTeacher" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export Report</h4>
                </div>
                <form name="frm" id="frm" method="post"
                      action="get_attendance_report.php?time=<?php echo time(); ?>&roll_id=student">
                    <div class="modal-body">
                       <div class="form-group">
                            <label for="usr">Select Batch</label>
                            <select name="branch_name1" id="branch_name1" class="form-control selectpicker" required="required" >
                                <?php
                                foreach ($resBatch as $value) {
                                    echo '<option value="' . $value[1] . '">' . $value[1] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="usr">Select Student</label>
                            <select name="roll_number" id="roll_number" class="form-control selectpicker" required="required" >
                              <option value="">Select Student</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input value="Export" class="btn btn-primary" type="submit"/>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div id="myModalBatch" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export Weekly Attendance Report</h4>
                </div>
				<?php if($_REQUEST['flag']=="error"){?>
				<div class="alert-danger">No Attendance data found for batch <?php echo $_REQUEST['batch']?></div>
				<?php }?>
                <!-- <form name="frm" id="frm" method="post"
                      action="exportAttendance.php?time=<?php echo time(); ?>&roll_id=student"> -->
					  <form name="frm" id="frm" method="post"
                      action="get_attendance_report_week.php">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">Select Batch</label>
                            <select name="branch_name" class="form-control selectpicker" required="required">
                                <?php
                                foreach ($resBatch as $value) {
                                    echo '<option value="' . $value[1] . '">' . $value[1] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
					 <div class="modal-body">
                        <div class="form-group">
                            <label for="usr">From Date:</label>
                            <input type="date" class="form-control" id="from_date"
                                   min="<?php echo $min = date( 'd-m-Y' ); ?>" name="from_date" class="datepicker"
                                   required/>
                        </div>&nbsp;&nbsp;<div class="form-group">
                            <label for="usr">To Date:</label>
                            <input type="date" class="form-control" id="to_date"
                                   max="<?php echo $min = date( 'd-m-Y' ); ?>" name="to_date" class="datepicker"
                                   required/>
                        </div>
                    </div>
                   
                    <div class="modal-body">
                        <div class="form-group">
                            <input value="Export" class="btn btn-primary" type="submit"/>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

<?php
//View Report
function getAllTeacherAction($link)
{
    $data = array();
    $sql = "SELECT roll_number, CONCAT(firstname,' ',lastname) AS fname FROM student WHERE status=1";
    $result = mysqli_query( $link, $sql );
    while ($rec = mysqli_fetch_row( $result )) {
        $data[] = $rec;
    }
    return $data;
}


//View Report
function getAllBatchAction($link,$member_type,$batches='')
{
	$cond="";
if($member_type=="U")
{
  $arr=explode(",", $batches);
$str="";
for($i=0;$i<count($arr);$i++)
{
	if($str=="")
	   $str="'".$arr[$i]."'";
	else
		$str.=",'".$arr[$i]."'";
}
$cond=" and branch_name in (".$str.")"; 
}
    $data = array();

	
		
   $sql = "SELECT id, branch_name FROM `batch` Where status=1".$cond;
    $result = mysqli_query( $link, $sql );
    while ($rec = mysqli_fetch_row( $result )) {
        $data[] = $rec;
    }
    return $data;
}

?>
    <script type="text/javascript">
	 $('#branch_name1').change(function(){	
			   document.getElementById('roll_number').options.length=0;
	  var batch;
    batch=this.value;
	  //var cid=$(this).value;
	  //alert("http://192.168.100.121/pleasechef-dev/chef/delete_cuisine.php?id="+cid+"&flag=disable");
	 // alert("list_students.php?batch="+batch);
	  $.ajax({
                            type: "POST",
                            url: "list_students.php?batch="+batch,    success: function(data){	
								//alert(data);
								if(data.match(/0/))
								{
								var subCatArr;
								var subCatArr1;
									var subCatSelect = document.getElementById('roll_number');	
									subCatArr = data.split("|");
		
			 //if(subCatArr!="")
			//{
			// addOption( subCatSelect, "", "Select Student") ;
			 
		     	
			   for( var j = 0 ; j < subCatArr.length ; j++ )
		       { 		
			     subCatArr1=subCatArr[j].split("~");
				 //alert(subCatArr1=subCatArr[j].split("~"))

				// alert(subCatArr1[0].replace(/^\s+|\s+$/,''))	
			     addOption( subCatSelect, subCatArr1[0].replace(/^\s+|\s+$/,''), subCatArr1[1].replace(/^\s+|\s+$/,''));
				// if(subcatid!="")
				 /*{
				   if((subCatArr1[0].replace(/^\s+|\s+$/,''))==subcatid)
				     subCatSelect.selectedIndex=j+1;
				  }*/
		       }
                                
                            }
							
							}
   
          });
		   });
		    function addOption( selectbox, value, text)
{
	var optn = document.createElement("OPTION");
	optn.value = value;

	if(value!="")
	optn.text = text+' ('+value+')' ; 
	else
		optn.text = text ; 
	//alert(value + " >>>>> " + text);
	selectbox.options.add(optn);	//if(subcatid!="")
	  
}
	 /*$(document).ready(function () {
		<?php if($_REQUEST['flag']=="error") {?>
			$('#myModalBatch').modal('show');
			<?php }?>
		 });
        $(document).ready(function () {
			
            $('.datepicker').datepicker({
                format: 'mm-dd-yyyy',
                endDate: '+0d',
                autoclose: true
            });
        });
        $(document).ready(function () {
            $("#frm").validate({
                $('.datepicker').datepicker({
                format: 'mm-dd-yyyy',
                endDate: '+0d',
                autoclose: true
            });
        });
        });*/

    </script>
<?php include_once './includes/footer.php'; ?>