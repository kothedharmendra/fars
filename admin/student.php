<?php
session_start();
require_once './config/config.php';
require_once 'includes/auth_validate.php';

//Get Input data from query string
$search_string = filter_input(INPUT_GET, 'search_string');
$filter_col = filter_input(INPUT_GET, 'filter_col');
$order_by = filter_input(INPUT_GET, 'order_by');
//Get current page.
$page = filter_input(INPUT_GET, 'page');
//Per page limit for pagination.
$pagelimit = 20;
if (!$page) {
    $page = 1;
}
// If filter types are not selected we show latest added data first
if (!$filter_col) {
    $filter_col = "created_at";
}
if (!$order_by) {
    $order_by = "Desc";
}

// select the columns
$select = array('stud_id', 'firstname','middlename','lastname','branch_name','roll_number','email','contact_number',
    'parent_contact_number','address','category','adhar_number','created_at','updated_at','status'
);
//$db->where('is_delete','0');


//Start building query according to input parameters.
// If search string
if ($search_string) 
{
    $db->where('firstname', '%' . $search_string . '%', 'like');
    $db->orwhere('middlename', '%' . $search_string . '%', 'like');
    $db->orwhere('lastname', '%' . $search_string . '%', 'like');
    $db->orwhere('roll_number', '%' . $search_string . '%', 'like');
    $db->orwhere('email', '%' . $search_string . '%', 'like');
    $db->orwhere('contact_number', '%' . $search_string . '%', 'like');
    $db->orwhere('status', '%' . $search_string . '%', 'like');
}
    //$db->where('is_delete','0');


//Set pagination limit
$db->pageLimit = $pagelimit;

//Get result of the query.
$student = $db->arraybuilder()->paginate("student", $page, $select);
$total_pages = $db->totalPages;

// get columns for order filter
foreach ($student as $value) {
    foreach ($value as $col_name => $col_value) {
        $filter_options[$col_name] = $col_name;
    }
    //execute only once
    break;
}
include_once 'includes/header.php'; ?>
<!--Main container start-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <h1 class="page-header">Manage Student</h1>
        </div>
        <div class="col-lg-6" style="">
            <div class="page-action-links text-right">
	            <a href="add_student.php?operation=create">
	            	<button class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add new </button>
	            </a>
            </div>
        </div>
    </div>
        <?php include('./includes/flash_messages.php') ?>
    <!--    Begin filter section-->
    <div class="well text-center filter-form">
        <form class="form form-inline" action="">
            <label for="input_search">Search</label>
            <input type="text" class="form-control" id="input_search" name="search_string" value="<?php echo $search_string; ?>">
            <!--<label for ="input_order">Order By</label>
            <select name="filter_col" class="form-control">
                <?php
                foreach ($filter_options as $option) {
                    ($filter_col === $option) ? $selected = "selected" : $selected = "";
                    echo ' <option value="' . $option . '" ' . $selected . '>' . $option . '</option>';
                }
                ?>
            </select>
            <select name="order_by" class="form-control" id="input_order">
                <option value="Asc" <?php
                if ($order_by == 'Asc') {
                    echo "selected";
                }
                ?> >Asc</option>
                <option value="Desc" <?php
                if ($order_by == 'Desc') {
                    echo "selected";
                }
                ?>>Desc</option>
            </select>-->
            <input type="submit" value="Go" class="btn btn-primary">
        </form>
    </div>
<!--   Filter section end-->
    <hr />
    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th class="header">#</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Batch</th>
                <th>Roll Number</th>
                <th>Contact Number</th>
                <th>Parent Contact Number</th>
                <th>Email</th>
                <th>Address</th>
                <th>Category</th>
                <th>Aadhar Number</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($student as $row) { ?>
                <tr>
	                <td><?php echo $row['stud_id'] ?></td>
	                <td><?php echo $row['firstname']; ?></td>
	                <td><?php echo $row['middlename']; ?></td>
	                <td><?php echo $row['lastname']; ?></td>
	                <td><?php echo $row['branch_name']; ?></td>
	                <td><?php echo $row['roll_number'] ?></td>
	                <td><?php echo $row['contact_number'] ?> </td>
	                <td><?php echo $row['parent_contact_number'] ?> </td>
                    <td><?php echo $row['email'] ?> </td>
                    <td><?php echo $row['address'] ?> </td>
	                <td><?php echo $row['category'] ?> </td>
	                <td><?php echo $row['adhar_number'] ?> </td>
	                <td><?php echo ($row['status']==1)? 'Enable' : 'Disable'; ?> </td>
	                <td>
					<a href="edit_student.php?student_id=<?php echo $row['stud_id'] ?>&operation=edit" class="btn btn-primary" style="margin-right: 8px;">
                        <span class="glyphicon glyphicon-edit"></span>
					<a href=""  class="btn btn-danger delete_btn" data-toggle="modal" data-target="#confirm-delete-<?php echo $row['stud_id'] ?>" style="margin-right: 8px;"><span class="glyphicon glyphicon-trash"></span></td>
				</tr>
            		<!-- Delete Confirmation Modal-->
					 <div class="modal fade" id="confirm-delete-<?php echo $row['stud_id'] ?>" role="dialog">
					    <div class="modal-dialog">
					      <form action="delete_student.php" method="POST">
					      <!-- Modal content-->
						      <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Confirm</h4>
						        </div>
						        <div class="modal-body">
						        		<input type="hidden" name="del_id" id = "del_id" value="<?php echo $row['stud_id'] ?>">
						          <p>Are you sure you want to delete this student?</p>
						        </div>
						        <div class="modal-footer">
						        	<button type="submit" class="btn btn-default pull-left">Yes</button>
						         	<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
						        </div>
						      </div>
					      </form>
					      
					    </div>
  					</div>
            <?php } ?>
            <?php if(empty($student)) :?>
                <tr>
                    <th colspan="14">&nbsp;</th>
                </tr>
                <tr>
                    <th colspan="14">No Record Found!</th>
                </tr>
            <?php endif;?>
        </tbody>
    </table>
<!--    Pagination links-->
    <div class="text-center">

        <?php
        if (!empty($_GET)) {
            //we must unset $_GET[page] if previously built by http_build_query function
            unset($_GET['page']);
            //to keep the query sting parameters intact while navigating to next/prev page,
            $http_query = "?" . http_build_query($_GET);
        } else {
            $http_query = "?";
        }
        //Show pagination links
        if ($total_pages > 1) {
            echo '<ul class="pagination text-center">';
            for ($i = 1; $i <= $total_pages; $i++) {
                ($page == $i) ? $li_class = ' class="active"' : $li_class = "";
                echo '<li' . $li_class . '><a href="student.php' . $http_query . '&page=' . $i . '">' . $i . '</a></li>';
            }
            echo '</ul></div>';
        }
        ?>
    </div>
    <!--    Pagination links end-->

</div>
<!--Main container end-->
<?php include_once './includes/footer.php'; ?>